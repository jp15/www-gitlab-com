---
title: "How to streamline your HIPAA risk analysis requirements with GitLab"
author: Luka Trbojevic
author_gitlab: ltrbojevic
author_twitter: gitlab
categories: security
image_title: '/images/blogimages/security-cover-new.png'
description: "How GitLab can help with 45 CFR § 164.308(a)(1)(ii)(A) – also known as the HIPAA risk analysis."
tags: features, security, testing
twitter_text: "How can @gitlab help with #HIPAA risk analysis? Find out!"
postType: product
---

The importance of the HIPAA risk analysis can’t be understated.
The Office for Civil Rights (OCR) announced that 2018 was an [all-time record year
for HIPAA enforcement](https://www.hhs.gov/hipaa/for-professionals/compliance-enforcement/agreements/2018enforcement/index.html), and an incomplete risk analysis or inadequate follow-up on findings were cited in three of the major breaches.

Digitization of healthcare is moving faster than ever. From patient portals to patient-reported outcomes platforms,
there’s an application for just about everything. But as we adjust our pace of building and
innovating in this digital healthcare era, we must quickly recalibrate our pace of identifying
risks and vulnerabilities in our software.

You may already know, GitLab is a single tool for the entire DevOps lifecycle, from project
planning to security scanning. But it’s also a powerful compliance and security tool.

Using built-in security tools, you can create a streamlined development process that natively
incorporates vulnerability scanning, helping to satisfy the HIPAA risk analysis requirement and secure the application.

Let’s take a closer look.

## Using Static Application Security Testing to identify vulnerabilities in source code

Using [Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/project/merge_requests/sast.html), you can
identify vulnerabilities in your source code. Setting up SAST is easy – you can either include the
[SAST CI job](https://docs.gitlab.com/ee/ci/examples/sast.html) or use Auto SAST.
After that’s done, and every time the job is run, your source code will be scanned.
When the scan is done, the [results are[displayed right on the merge request](https://docs.gitlab.com/ee/user/project/merge_requests/sast.html#how-it-works). And when you go to any pipeline with a SAST job, you’ll be shown a [security report](https://docs.gitlab.com/ee/user/project/merge_requests/sast.html#security-report-under-pipelines) with the findings.

## Using Dynamic Application Security Testing to identify vulnerabilities in web applications

Unlike SAST, which scans source code for vulnerabilities, [Dynamic Application Security Testing (DAST)](https://docs.gitlab.com/ee/user/project/merge_requests/dast.html)
analyzes running web applications for vulnerabilities.
It’s just as simple to set up as SAST – simply add a DAST CI/CD job to your pipeline.
DAST will also [display the findings directly in the merge request](https://docs.gitlab.com/ee/user/project/merge_requests/dast.html#how-it-works)
and create a [report artifact](https://docs.gitlab.com/ee/ci/yaml/README.html#artifactsreportsdast-ultimate).

## Container Scanning

If you use Docker, you can use [Container Scanning](https://docs.gitlab.com/ee/ci/examples/container_scanning.html)
to scan your Docker images for vulnerabilities. Again, it's as simple as adding a [Container Scanning CI/CD job](https://docs.gitlab.com/ee/ci/examples/container_scanning.html#configuring-with-templates)
to your pipeline! The scan will generate a [report artifact](https://docs.gitlab.com/ee/ci/yaml/README.html#artifactsreportscontainer_scanning-ultimate) you can download and review.

## Secret Detection

The risk analysis standard requires both risks and vulnerabilities. One common risk is for
secrets (API keys and passwords, for example) to be inadvertently leaked. To address that problem,
we’re working on [Secret Detection](https://gitlab.com/groups/gitlab-org/-/epics/675).
It’ll check files and configurations to identify potentially sensitive information, running every time a commit is pushed to a branch.

## Coming soon: Even more tools to assess risks and vulnerabilities

In the coming year we’ll be adding a number of product categories to our Secure stage to help
improve your application’s security and find more vulnerabilities. Here’s what you can look forward to:

### Digging deeper for application vulnerabilities: Interactive Application Security Testing

[Interactive Application Security Testing (IAST)](https://gitlab.com/groups/gitlab-org/-/epics/344) assesses an application’s response
to an external security scan (like DAST) to identify vulnerabilities that wouldn’t be caught by
just the external scan. When this feature is complete, it’ll add yet another layer of vulnerability detection to DAST.

### Fuzzing

Another way to find application vulnerabilities is to generate random inputs and send them
to the application. By doing this, you can find unintended behaviors in the application that
may result in a vulnerability. While fuzzing is often a niche technique, we’re
[working on adding basic fuzzing capability straight into GitLab](https://gitlab.com/groups/gitlab-org/-/epics/818)!

## Putting it all together

Today, with GitLab, you can:

* Identify vulnerabilities in your source code using SAST
* Identify vulnerabilities in your web application using DAST
* Identify vulnerabilities in your Docker containers using Container Scanning

In the near future, with GitLab, you’ll be able to:

* Scan for passwords, API keys, and other sensitive information with Secrets Detection
* Identify vulnerabilities in your application using IAST
* Identify vulnerabilities in your application with fuzzing

Individually or together, these features can help meet the HIPAA risk analysis requirement.

## Closing thoughts

Whether you’re a four-person startup making the next groundbreaking healthcare analytics
platform or an academic medical center developing health applications, having security visibility
where it didn’t exist previously is a good thing. And having that visibility incorporated directly
into your development process with minimal work and seamless integration is even better.

HIPAA compliance is a challenge for any organization, but with GitLab’s security features you
can incorporate the risk analysis requirement straight into your development process.
While the risk analysis requirement goes beyond the software, as you write more code faster,
automating the software part of the equation can only help.
