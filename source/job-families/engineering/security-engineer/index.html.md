---
layout: job_family_page
title: "Security Engineer"
---

As a member of the security team at GitLab, you will be working towards raising the bar on security. We will achieve that by working and collaborating with cross-functional teams to provide guidance on security best practices.

The [Security Team](/handbook/engineering/security) is responsible for leading and
implementing the various initiatives that relate to improving GitLab's security.

## Responsibilities

- Develop security training and guidance to internal development teams
- Provide subject matter expertise on architecture, authentication and system security
- Assess security tools and integrate tools as needed, particularly open-source tools
- Assist with recruiting activities and administrative work
- Technical Skills
  * Familiar with common security libraries, security controls, and common security flaws that apply to Ruby on Rails applications.
  * Ability to discover and patch SQLi, XSS, CSRF, SSRF, authentication and   authorization flaws, and other web-based security vulnerabilities (OWASP Top 10 and beyond).
  * Knowledge of common authentication technologies including OAuth, SAML, CAs, OTP/TOTP.
  * Knowledge of browser-based security controls such as CSP, HSTS, XFO.
  * Experience with standard web application security tools such as Arachni, Brakeman, and BurpSuite.
  * There should also be time to participate in development of GitLab.
- Code quality
  * Proactively identify and reduce security risks.
  * Find and remove outdated and vulnerable code and code libraries.
- Communication
  * Consult with other Developers and Product Managers to analyze and propose application security standards, methods, and architectures.
  * Handle communications with independent vulnerability researchers and design appropriate mitigation strategies for reported vulnerabilities.
  * Educate other developers on secure coding best practices.
  * Ability to professionally handle communications with outside researchers, users, and customers.
  * Ability to communicate clearly on technical issues.
- Performance & Scalability
  * An understanding of how to write code that is not only secure but scales to a large number of users and systems.

## Requirements

- You have a passion for security and open source
- You are a team player, and enjoy collaborating with cross-functional teams
- You are a great communicator
- You employ a flexible and constructive approach when solving problems
- You share our [values](/handbook/values), and work in accordance with those values

## Levels

### Intermediate Security Engineer

* Leverage understanding of fundamental security concepts
* Triages/handles basic security issues
* Be positive and solution oriented
* Good written and verbal communication skills
* Constantly improve product security

### Senior Security Engineer

The Senior Security Engineer role extends the [Intermediate Security Engineer](#intermediate-security-engineer) role.

* Leverages security expertise in at least one specialty area
* Triages and handles/escalates security issues independently
* Conduct security architecture reviews and makes recommendations
* Great written and verbal communication skills
* Screen security candidates during hiring process

***

A Senior Security Engineer may want to pursue the [security engineering management track](/job-families/engineering/security-management) at this point. See [Engineering Career Development](/handbook/engineering/career-development) for more detail.

***


### Staff Security Engineer

The Staff Security Engineer role extends the [Senior Security Engineer](#senior-security-engineer) role.

* Recognized security expert in multiple specialty areas, with cross-functional team experience
* Make security architecture decisions
* Provide actionable and constructive feedback to cross-functional teams
* Implement security technical and process improvements
* Exquisite written and verbal communication skills
* Author technical security documents
* Author questions/processes for hiring and screening candidates
* Write public blog posts and represent GitLab as a speaker at security conferences

## Specialties

### Application Security

Application Security specialists work closely with development, product security PMs, and third-party groups (including paid bug bounty programs) to ensure pre and post deployment assessments are completed. Initiatives for this specialty also include:

* Pre and post deployment security assessments/tests
* Capture of flaws in software environment configuration
* Malicious code detection
* Patch/upgrade
* IP filtering
* Lock down executables
* Monitoring of programs at runtime to enforce the software use policy
* Developing and implementing a Secure Software Development Lifecycle (S-SDLC)
* Training and coaching developers on current security best practices

#### Application Security Responsibilities

- Own vulnerability management and mitigation approaches
- Conduct threat modeling tied to security services
- Conduct application security reviews
- Implement secure architecture design
- Provide security training and outreach to internal development teams
- Develop security guidance documentation
- Assist with recruiting activities and administrative work
- Define, implement, and monitor security measures to protect GitLab.com and company assets

#### Application Security Requirements

- Familiarity with common security libraries, security controls, and common security flaws that apply to Ruby on Rails applications
- Some development experience (Ruby and Ruby on Rails preferred; for GitLab debugging)
- Experience with OWASP, static/dynamic analysis, and common exploit tools and methods
- An understanding of network and web related protocols (such as, TCP/IP, UDP, IPSEC, HTTP, HTTPS, routing protocols)
- Familiarity with cloud security controls and best practices

### Security Automation

Security Automation specialists help us scale by creating tools that perform common tasks automatically. Examples include building automated security issue triage and management, proactive vulnerability scanning, and defining security metrics for executive review. Initiatives for this specialty also include:

* Assist other security specialty teams in their automation efforts
* Assess security tools and integrate tools as needed
* Define and own metrics and KPIs to determine the effectiveness of security programs
* Define, implement, and monitor security measures to protect GitLab.com and company assets

#### Security Automation Responsibilities

- Build security tooling and automation for internal use that enable the security team to operate at high speed and wide scale
- Define and own metrics and key performance indicators to determine the effectiveness of security programs
- Define, implement, and monitor security measures to protect GitLab.com and company assets

#### Security Automation Requirements

- Previous experience on a Security Operations team, especially experience coordinating responses to security incidents
- Scripting/coding experience with one or more languages
- Extensive knowledge of Internet security issues, cloud architectures, and threat landscape
- Solid understanding of the Software as a Service (SaaS) model
- Experience with Cloud Computing Platforms - GCP experience a plus

Security Engineers at GitLab work on securing our product and on internal security. On the product side, this includes the open source version of GitLab, the enterprise editions, and the GitLab.com service. Security Engineers work with peers on cross-functional teams dedicated to areas of the product. They also work together with product managers, developers, and the infrastructure teams to solve common goals.

### Security Operations

Security Operations specialists respond to incidents. This is often a fast-paced and stressful environment, where responding quickly and maintaining ones composure is critical. Initiatives for this specialty also include:

* Must have strong skills and experience in at least one of the areas below:
  * Vulnerability management
  * Incident response
  * Logging and analytics
* Focus on detection and incident response, as well as implement preventative mechanisms
* Coordinate company-wide responses to security incidents
* Incorporate current security trends, advisories, publications, and academic research
* Engineer CND technologies to monitor and analyze e.g.
  *  IDSes
  *  Data collection tools
* Conduct vulnerability management
* Identify and mitigate complex security vulnerabilities before an attacker exploits them
* Determine level of effort required to compromise sensitive data
* Triage and manage vulnerabilities identified through scanning

### Abuse Operations

Abuse Operations specialists investigate malicious use of our systems. Initiatives for this specialty include:

* Three main types of abuse
  *  Continuous Integration
  *  Copyright violations
  *  Port scanning
* Also investigate reports such as spam email, DoS, etc.
* Take action on abusive/non-responsive customers
* Verify proper classification of incoming abuse reports
* Execute messaging to customers on best practices
* Escalate to other stakeholders while continuing to monitor
* Monitor logs and queues for trends
* Research and prevent trending abuse methodologies

### Compliance

Compliance specialists enables Sales by achieving standard as required by our customers. This includes SaaS, on-prem, and open source instances. Initiatives for this specialty also include:

* Develop roadmap based on customer needs e.g.
  *  GDPR
  *  SOC 2
  *  FIPS 140-2
* Align other security specialist activities with the compliance roadmap
* Develop relationships with key government personnel and policy makers
* Assist work of internal and external auditors or advisors as needed

### Red Team

Red Team specialists emulate adversary activity to better GitLab’s enterprise and product security. The role requires the ability to think like an advanced persistent threat. Creativity is key. For example, develop attack plans and stealthily execute them to compromise sensitive information on GitLab.com such as private repos, or develop and distribute malware to GitLabbers to demonstrate how the corporate enterprise could be compromised.

* Utilize threat modeling concepts and frameworks such as MITRE ATT&CK, STRIDE, etc. to continually identify ways to protect and defend GitLab assets by executing attacks that emulate a range of adversaries
* Focus on designing, researching, and executing attacks to challenge the blue team
* Strive to identify weaknesses within GitLab products and corporate network and demonstrate the associated risks
* Contribute to the GitLab Secure and Defend products
* Incorporate current security trends, advisories, publications, and academic research
* Understand CND technologies to bypass these security controls and stay undetected
* Report on the Red Team engagements providing an in-depth analysis of the security issues identified
* Identify complex security vulnerabilities and exploit them before an external attacker can exploit them
* Determine the level of effort required to compromise sensitive data
* Publish blog posts and present talks at security conferences
* Contribute to GitLab products by testing and proposing new features

### Strategic Security

Strategic security specialists focus on holistic changes to policy, architecture, and processes to reduce entire categories of future security issues. Initiatives for this specialty also include:

* Cluster related historical security issues and examine them as a larger set
* Identify and generate trends associated with each set
* Propose actionable changes to GitLab architecture, processes, and infrastructure to mitigate future issues within each set
* Generate metrics which measure the effectiveness of each mitigation implemented

### Security Research

Security research specialists conduct internal testing against GitLab assets, and against FOSS that is critical to GitLab products and operations. Initiatives for this specialty also include:

* Conduct vulnerability research against all GitLab and GitLab.com assets
* Research FOSS tools that are integrated with GitLab
* Develop proof-of-concept code to be included in security findings
* Report findings to tool developers and track mitigation process
* Follow responsible disclosure policies for community disclosure
* Author blog posts on vulnerabilities discovered

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

### Screening call with Recruiter

### Round 1 (Scheduled in Parallel)
- 60 Minute Interview with Hiring Manager
- 45 Mintue Senior Peer Interview

### Round 2 (Scheduled in Parallel) 
- 60 Minute Interview with Director of Security
- 60 Minute Interview with VP of Engineering  
- Successful candidates will subsequently be made an offer via email

As always, the interviews and screening call will be conducted via a video call.
See more details about our hiring process on the [hiring handbook](/handbook/hiring).
