---
layout: job_family_page
title: "Product Manager"
---

## Role

Product Managers at GitLab have a unique opportunity to define the future of the
entire [DevOps lifecycle](https://about.gitlab.com/stages-devops-lifecycle/). We
are working on a [single application](/handbook/product/single-application) that
allows developers to invent, create, and deploy modern applications.

We want to facilitate onboarding to [Concurrent DevOps](https://about.gitlab.com/concurrent-devops/):
users may not be aware of its potential, we will show them how easy and powerful
it is! We want to support large teams working on mission-critical software
across the entire company, and also single developers starting a new visionary
project from scratch.

We work in a very unique way at GitLab, where flexibility and independence meet
a high paced, pragmatic way of working. And everything we do is [in the open](https://about.gitlab.com/handbook).

We are looking for talented product managers that are excited by the idea to
contribute to our vision. We know there are a million things we can and want to
implement in GitLab. Be the one making decisions.

We encourage people to apply even if they don't have an established background
in this role yet, but they feel this page describes exactly what they have a
strong attitude for. Product managers always start off somewhere, and it's ok if
that somewhere is GitLab.

We recommend looking at our [about page](/about) and at the [product handbook](https://about.gitlab.com/handbook/product/)
to get started.

<a id="base-pm-requirements"></a>

### Responsibilities

- **Drive the product in the right direction**
  - Build an effective [roadmap](/handbook/product/#3-month-roadmap) to
    prioritize important features properly
  - Take high-level feature proposals and customer problems and break them into [small iterations](/handbook/values/#iteration)
    that engineering can work on
  - [Balance](/handbook/product/#prioritization) new features, improvements, and
    bugfixes to ensure a high velocity and a stable product
  - Consider the business impact, [ROI](https://en.wikipedia.org/wiki/Return_on_investment),
    and other implications when taking important decisions

- **Take an active role in defining the future**
  - Contribute to the [product vision](/direction/#vision), together with the
    Head of Product and VP of Product
  - Create and maintain a [vision for your product area](/handbook/product/#stage-vision)
  - Create and maintain the [vision for each product category](/handbook/product/#category-vision)
  - Innovate within your product area by proposing [ambitious](/handbook/product/#be-ambitious)
    features
  - Follow innovation in your product area
  - Communicate and evangelize your product vision internally and among the
    wider community

- **Manage the product lifecycle end-to-end**
  - Follow feature development end-to-end; provide guidance and feedback to
    engineers and designers; ensure everyone is aligned
  - Be the voice of the customer and the [subject-matter expert](https://en.wikipedia.org/wiki/Subject-matter_expert)
    for your group
  - Contribute to documentation, blog posts, demos, and marketing materials for
    product features
  - Collaborate with other Product Managers, UX, and engineers in cross-area
    features to build a [single application](/handbook/product/single-application)
  - Manage the
    [uncertainty](https://www.cleverpm.com/2018/08/23/accepting-uncertainty-is-the-key-to-agility/)
    in an efficient way, adjusting plans to new working conditions
  - Drive teams to acquire necessary data in order to back assumptions, show value, and drive prioritization

- **Engage with stakeholders in two-way communication**
  - Assist Sales, Support, Customer Success, and Marketing as the [subject-matter expert](https://en.wikipedia.org/wiki/Subject-matter_expert)
    for your area
  - [Talk to customers](/handbook/product/#customer-meetings) and engage with
    the community regularly
  - Engage with analysts on briefings and product evaluations
  - Work with the entire Product team to share improvements and best practices

### You are _not_ responsible for

- **A team of engineers:** you will take the lead in decisions about the
  product, but not manage the people implementing it
- **Capacity planning:** you will define priorities, but the Engineering Manager
  evaluates the amount of work possible
- **Shipping in time:** you will work in a group, but the group is responsible
  for shipping in time, not you

### Requirements

- Experience in product management
- Strong understanding of Git and Git workflows
- Knowledge of the developer tool space
- Strong technically: you understand how software is built, packaged, and deployed
- Passion for design and usability
- Highly independent and pragmatic
- Excellent proficiency in English
- You are living wherever you want and are excited about the [all remote](https://about.gitlab.com/company/culture/all-remote/) lifestyle
- You share our [values](/handbook/values), and work in accordance with those values
- Bonus points: experience with GitLab
- Bonus points: experience in working with open source projects

***

## Career paths

### In role levels

Read more about [levels](/handbook/hiring/#definitions) at GitLab here.

#### Intermediate Product Manager

Product Manager requirements and responsibilities are outlined [above](#base-pm-requirements).

#### Senior Product Manager

The Senior Product Manager role extends the [Product Manager](#base-pm-requirements) role. Senior Product Managers are expected to be experts in their product domain and viewed as such to the community and internally at GitLab. They are expected to prioritize and manage their products with minimal guidance from leadership or other product team members.

##### Responsibilities

**Drive the product in the right direction**
* Consistently deliver outsized impact to [IACV](https://about.gitlab.com/handbook/finance/operating-metrics/#incremental-annual-contract-value-iacv) or other [Product and CEO KPIs](https://about.gitlab.com/handbook/ceo/kpis/)

**Take an active role in defining the future**
* Mastery of the competitive and market landscape of their product domain and understanding how this landscape impacts the product roadmap

**Manage the product lifecycle end-to-end**
* Document ROI or impact for a given action, feature, or prioritization decision
* Execute to deliver outsized results on the aforementioned ROI/impact analysis

**Engage with stakeholders in two-way communication**
* Represent GitLab as a product and domain expert in front of industry analysts, strategic customers, industry events/conferences, and other events
* Ability to present to C-level executives both internally at GitLab and externally to customers and prospects

**Lead by example**
* Mentor less experienced Product Managers to enable them add more value sooner

##### Requirements

* 5+ years product management experience

### Moving to and moving from

The career paths to and from product management positions are varied, but there are some common patterns.

#### Moving to Product Management

Successful product managers have a passion for solving customer needs. As a result they've
been found to start their journey to product management from the following disciplines:
* **Product Marketing**: Product Marketers with a desire to have more influence in shaping the product often 
migrate to the Product Management function. 
* **Customer Success**: Customer facing functions like Solutions Architects, Technical Account Managers, and 
lead Support techs who've developed a strong understanding of customers' needs in the product often transition
to Product Management out of a desire to shape the direction of the product.
* **UX**: UX designers and leaders have built a great understanding of the personas, and pain points which their product
is targeting. A desire to further influence not just the experience, but the way the features alleviate those
pain points can draw UX professionals to Product Management.
* **Engineering Manager**: Engineering team leaders who've gained a strong understanding of the customer persona, pain 
points and direction of a product often want to further influence that direction by transitioning to Product Management roles.
* **QA/QE**: Quality engineers are responsible for the validation of a product and service. Those who 
have a passion for understanding and testing how products are used in the real world by real users often
find a strong alignment to the Product Management function. 

#### Moving from Product Management

Within their role, Product Managers are empowered to interact and learn more about functions they are interested in. 
Whether that be Marketing, Customer Success, Support, Finance or Engineering - Product Managers are encouraged (and often 
required) to understand other functions as part of their daily responsibilities. As such there is plenty of room for
Product Managers to transition to roles outside of the Product Management team. Some of those include:
* **Engineering Leadership**: Product Managers often find a desire to dive deeper into Engineering problems, or grow
mentor and lead engineering teams. 
* **Product Marketing**: Product Managers often gravitate to how we target, message and deliver value to our customers. 
Product Marketing roles are an excellent way to further that expertise.
* **Customer Success**: Few people know our products as well as Product Managers, and product managers who enjoy directly
solving customers problems on a daily basis make greaet fits for Customer Success roles.
* **General Management**: As a result of their exposure to a wide variety of functions, Product Managers often make a 
transition from Product Management to General Management.

## Specialties

### Verify (CI)

We're looking for product managers that can help us work on the future of DevOps tools; specifically, building out continuous integration (CI), code quality analysis, micro-service testing, usability testing, and more.

#### Requirements

- Strong understanding of DevOps, CI/CD, and Release Automation
- Practical understanding of container technologies including Docker and Kubernetes

### Release (CD)

We're looking for product managers that can help us work on the future of DevOps tools; specifically, building out continuous delivery (CD), release orchestration, features flags, and more.

#### Requirements

- Strong understanding of DevOps, CI/CD, and Release Automation
- Understanding of deployment infrastructure and container technologies
- Practical understanding of container technologies including Docker and Kubernetes

### Configure

We're looking for product managers to help us work on the future of DevOps tools; specifically, building out configuration management and other operations-related features such as ChatOps.

#### Requirements

- Strong understanding of CI/CD, configuration management, and operations
- Understanding of deployment infrastructure and container technologies
- Practical understanding of container technologies including Docker and Kubernetes

### Serverless

We're looking for product managers to help us work on the future of DevOps tools; specifically, building out serverless application and function management.

#### Requirements

- Strong understanding of DevOps and cloud-native application development
- Understanding of deployment infrastructure and serverless functions
- Practical understanding of container technologies including Docker and Kubernetes

### Package

We're looking for product managers to cover the [Package stage](/handbook/product/categories/#package) of our DevOps lifycle. This candidate will work specifically on building out packaging categories and features such as Docker container registry and binary artifact management.

#### Requirements

- Strong understanding of CI/CD and package management
- Understanding of the complexity of managing multi-artifact application deployment
- Practical understanding of deployment infrastructure and container technologies including Docker and Kubernetes

### Distribution

We're looking for product managers to support our [Distribution group](/handbook/product/categories/#admin) and manage our installation, upgrade, and configuration process for our self-managed customers. 

#### Requirements

- Strong understanding of system administration
- Practical understanding of deployment infrastructure and container technologies including Docker and Kubernetes

### Secure

We're looking for product managers that can help us work on the future of developer tools; specifically, building out application security testing, including static analysis and dynamic testing.

#### Requirements

- Strong understanding of CI/CD and automated security testing
- Practical understanding of deployment infrastructure and container technologies including Docker and Kubernetes

### Defend

- Strong understanding of SIEM and operational security
- Practical understanding of deployment infrastructure and container technologies including Docker and Kubernetes

### Growth

Get more people to use and contribute to GitLab, mainly the Community Edition (CE). Marketing gets us awareness and downloads. Your goal is to ensure people downloading our product keep using it, invite others, and contribute back. You report to the VP of Product.

#### Goals

1. Conversion of download to 30 day active (install success * retention)
1. Growth of existing CE installations
(growth of comments measured with usage ping)
1. Growth in the number of new people that contributed merged code

#### Possible Improvements

1. Auto install let's encrypt
1. Check open to new users by default
1. Check invite by email function
1. Usage ping for CE
1. Ensure installers are very simple
1. First 60 seconds with a product
1. Measure contributors like a SaaS funnel (activation, retention)
1. Improve data for Usage ping EE
1. Improve zero-states of project (hints on first action)
1. New feature show-off / highlighting
1. Great experience when not logged in (commenting)
1. Great experience when not a project member (edit button works)
1. Make it easier to configure email
1. Make it easier to configure LDAP

#### You'll work with

* Other PM's
* Developers (implement some things self, some with help of experts)
* Community team
* Developers
* Merge request coaches

#### Requirements

- Deep understanding of UX and UI
- You've built web applications before in Ruby on Rails (be it personal or professional)
- You're pragmatic and willing to code yourself
- You're able to independently find, report and solve issues and opportunities related to growth in the product
- Good understanding of Git
- Able to make wireframes and write clear, concrete product specifications

#### Responsibilities

- Plan and execute on improvements in GitLab related to growth
- Write specs and create wireframes to communicate your plans
- Ship improvements every month and make it possible to report on those
improvements
- Do data analysis whenever useful
- Assist the rest of the team with topics related to growth
- Build and expand tools related to growth (version.gitlab.com and others)

### Scaling

We're looking for a product manager to elevate the broader product team. Your goals are to ensure important inputs are effectively managed, tackle challenges that do not neatly fit within a stage, and do whatever is needed most.

#### Requirements

- Deep understanding of the full DevOps lifecycle and tools
- Excellent critical thinking skills; come up to speed quickly and determine a good solution
- Clear and concisely communicate to others in writing
- Strong organizational skills

#### Responsibilities

- Attend all Product:CEO and Product Development meetings, record and follow up on action items
- Effectively shepherd feedback from the executive team, primarily the CEO
- Investigate new initiatives and technologies which could impact GitLab
- Improve the process and tools to make the product team more effective
- Do whatever is needed most

## Relevant links

- [Product Handbook](/handbook/product)
- [Engineering Workflow](/handbook/engineering/workflow)

### Meltano (BizOps Product)

We're looking a product manager to shape the future of data science tools. You would be taking GitLab's approach to complete devops, and apply it to Meltano. Meltano is an open source convention-over-configuration framework for data science, analytics, business intelligence, and data science. We will leverage version control, CI, CD, Kubernetes, and review apps. We aim to help customers answer difficult questions e.g., how can I acquire the highest customer lifetime value (LTV) at the lowest customer acquisition cost (CAC). We are creating open source, deeply-integrated tools without using traditional BI tools.

It'll be your job to work out what we are going to do and how. We need a full-stack PM, i.e. someone who can write code, update pipelines, and do whatever it takes to get things done and shape the future of data science.

#### Requirements

- Strong understanding of business operations, business intelligence, data warehousing, etc.
- Experience with CI/CD and containers. Knowledge of Kubernetes and GitLab CI/CD a plus.
