--- 
layout: job_family_page
title: "Senior Director of Revenue Marketing"
---

Our Senior Director of Revenue Marketing is a driven, innovative and energetic leader, with a talent for empowering high performing teams and an ability to get things done. Reporting to the CMO, the Senior Director of Revenue Marketing is accountable for creating and executing a scalable marketing strategy to achieve aggressive new business pipeline and revenue targets. The Senior Director of Revenue Marketing plays an integral role in driving revenue growth for the company across all channels and geos. This is a highly visible global role in the organization, responsible for the development and implementation of effective demand generation programs with a strong emphasis on lead generation, pipeline creation and nurturing prospects/customers along the buyer’s journey.

The Senior Director of Revenue Marketing is an experienced sales and marketing leader with a proven track record of building high performance teams, in rapid-growth environments, to deliver both predictable and sustainable results.  He or she thrives in a fast-paced environment and has a reputation for creating alignment with sales and inspiring trust with sales, based on proven process and metrics. Field Marketing, Digital Marketing Programs and Sales Development (in-bound and outbound lead generation) report into this leader. 

He or she is accountable for KPIs and quotas that align directly to sales revenue goals and encompass all sources of pipeline generation. Top areas of focus are tracking the impact of marketing programs on pipeline values, average deal size and close rates globally and by segment.

Our Senior Director of Revenue Marketing has proven success collaborating with all sales segmentations and marketing teams to envision, pilot, execute, and measure closed-loop integrated marketing programs that create a sense of urgency for the buyer and deliver significant pipeline and revenue growth for the company.  A strong desire to create a model that is innovative in the industry is a must.

## Responsibilities
- Lead GitLab customer acquisition strategy and operational marketing plan to deliver on pipeline and revenue targets
- Set organization direction, staff for scale and develop the skills and career paths of all team members
- Serve as subject matter authority on engagement, performance, and Return on Investment metrics
- Lead and develop a team of marketing professionals to achieve key business metrics including MQLs, sales-accepted-opportunities, and closed business
- Accountability for meeting/exceeding attainment of sales pipeline goals and leading indicators of demand
- Forecast, measure, analyze and report on the impact of demand creation activities on sales pipeline and bookings. 
- Drive clear and constant communication of goals and success metrics to the organization to ensure consistency and accountability with end-to-end reporting and analytics
- Continuously evaluate pipe-to-spend and the performance and cost of acquisition, adjusting demand tactics and strategy accordingly
- Ensure highly productive relationship with Sales, forging strong communication and aligning on campaign and target account strategy, we are one team

## Requirements 
- 5 – 8 years of marketing experience with demonstrated success in online demand generation for technology companies for a Software SaaS based or cloud based companies
- Experience selling and/or marketing open source technology, sales leadership experience a big plus
- 8-10 years building, managing, and leading a sales development organization
- An established track record of successful lead generation, nurture, and integrated marketing campaigns
- Expertise in B2B online marketing tactics and marketing automation tools such as Marketo, Outreach, Drift, LeanData and Salesforce.com
- Proven track-record of success in partnering with Sales for common goals
- Data-driven marketing, demonstrated analytical ability and orientation for detail.
- Must be comfortable with a balance of day-to-day execution and high-level strategy across three disciplines with one common goal.
- Must understand the difference between a leader and a manager
- Experience managing external vendors and agencies
- Strong work ethic; takes the initiative and thinks creatively to get the job done

## Relevant Links
- [Marketing Handbook](/handbook/marketing)
