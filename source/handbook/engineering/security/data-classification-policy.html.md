---
layout: markdown_page
title: "GitLab Data Classification Policy"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# GitLab Data Classification Policy

Data classification provides a way to categorize organizational data based on levels of sensitivity. This includes understanding what data types are available, where the data is located, and access levels and protection of the data (for example, through encryption and access control). By carefully managing an appropriate data classification system along with each tierâs protection requirements, you can map the controls and level of access/protection appropriate to the data. For example, public-facing content is available for anyone to access, whereas critical content is encrypted and stored in a protected manner that requires authorized access to a key for decrypting the content.

## Data Classification levels

### RED

Data that falls in this category must remain confidential. This is our most sensitive data and access to it should be considered as âprivilegedâ. Exposure of this data to unauthorized parties could cause extreme loss to GitLab and/or its customers. In the gravest scenario, exposure of this data could trigger or cause a business extinction event. Requests for access to this type of data should be reviewed based on the principle of âneed-to-knowâ and approved by appropriate data owner. Once approved, access should be logged and monitored. Additional security measures should be put in place to ensure immediate response to access that seems unexpected or inappropriate. These measures should include a formalized and documented review of access on a recurring basis, during which appropriateness of access and access entitlements are re-certified. Strong access controls should be put in place to ensure that this type of data is not publicly exposed.

### ORANGE

This classification applies to data and information that should not be made generally available. Unauthorized access or disclosure could break contractual obligations, and/or adversely impact GitLab, its partners, employees, and customers. As with data within the RED classification, access to this data must be approved by appropriate owner prior to being granted, and once provisioned, access should be logged and monitored. Strong access controls should be put in place to ensure that this type of data is not publicly exposed.

Access to ORANGE data is limited to those with a business need-to-know, including third parties and customers, as defined by GitLab Security.

### YELLOW

This classification covers non-public data that is not classified as ORANGE or RED. Disclosure of this data could violate privacy policies, and/or adversely impact GitLab, its partners, employees, and customers. Access to this data should be approved prior to being provisioned and should be logged. Strong access controls should be put in place to ensure that this type of data is not publicly exposed.

Access to YELLOW data is limited to those with a business need-to-know, including third parties and customers, as defined by GitLab Security.

### GREEN

Data and information that are publicly shareable, and do not expose GitLab or its partners to any harm.

## Credentials and access tokens are classified at the same level as the data they protect

Credentials such as passwords, encryption keys, and session cookies derive their importance from the data they protect. For example, authentication cookies for GitLab.com super users are classified as RED because anyone who gains access to them will have access to customer content, which is RED data.

## Systems must be protected at a level appropriate for the volume of data they process.

A system that processes a single data element at a time still processes many over time and must therefore be protected at a level appropriate for systems processing bulk data.

Typically only client applications are considered to be handling "A single customer's" data. Data on hosts/services is typically always "more than one customer". (example of hosts running âclient applicationsâ would likely be runner hosts)

## Combinations of data types may result in a higher classification level

An example of this would be a single customerâs encrypted content (ORANGE) that is stored on the same system as a GitLab employeeâs credentials (ORANGE). If this user has permission to access to secrets that decrypt the customerâs encrypted content, this elevates the classification (to RED).

## Determining the appropriate classification is at the discretion of GitLab Security and/or Leadership

GitLab Security should be contacted for all requests to review data classification of new data types.

If you have data that is not specified in the [Data Classification Index (internal only)](https://docs.google.com/spreadsheets/d/1eNuSLuBcZWQe13SV1TfEjtNdCOZw7G7ofY9A42Y0sPA/edit#gid=0) or does not fit the current classification and handling guidelines, please contact GitLab Security.

The following guidelines are used to determine the classification of data. Until GitLab Security makes a formal determination of classification, use these guidelines to self-classify your data:
* If the data could cause a business extinction event or other irreparable loss or harm to the company if lost or exposed to unauthorized parties, it should be classified as RED. You must contact GitLab Security for a review and approval for any critical data.
* If the data could cause significant or financially material loss or risk of harm to GitLab if exposed to unauthorized parties, it should be classified as ORANGE.
* If the data would cause minimal risk of loss or harm to GitLab if exposed, it should be classified as YELLOW.
* If the data does not expose GitLab to any harm and can or should be made available to the public, it may be classified as GREEN.

If you cannot identify the data element in the [Data Classification Index (internal only)](https://docs.google.com/spreadsheets/d/1eNuSLuBcZWQe13SV1TfEjtNdCOZw7G7ofY9A42Y0sPA/edit#gid=0), or are uncertain of the risk associated with the data and how it should be classified and handled, please contact GitLab Security.
