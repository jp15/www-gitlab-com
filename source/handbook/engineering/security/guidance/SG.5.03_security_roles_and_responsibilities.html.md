---
layout: markdown_page
title: "SG.5.03 - Security Roles and Responsibilities Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# SG.5.03 - Security Roles and Responsibilities

## Control Statement

Roles and responsibilities for the governance of Information Security within GitLab are formally documented within the Information Security Management Standard and communicated on the GitLab intranet.

## Context

To be able to effectively work with the Security team at GitLab, knowing who is responsible for what is important in order to direct questions, concerns, and specific efforts to the right person(s). The purpose of this control is to ensure roles and responsibilities for the Security team are updated and kept current, and that the reporting structure within the department remains transparent.

## Scope

TBD

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLabbers, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SG.5.03_security_roles_and_responsibilities.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SG.5.03_security_roles_and_responsibilities.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SG.5.03_security_roles_and_responsibilities.md).

## Framework Mapping

* ISO
  * A.6.1.1
* SOC2 CC
  * CC1.1
  * CC1.4
  * CC1.5
  * CC2.2
  * CC2.3
* PCI
  * 1.1.5
  * 12.10.1
  * 12.4
  * 12.5
  * 12.5.1
  * 12.5.2
  * 12.5.3
  * 12.5.4
  * 12.5.5
