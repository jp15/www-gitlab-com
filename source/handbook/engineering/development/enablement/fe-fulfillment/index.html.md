---
layout: markdown_page
title: Front End Fulfillment Team
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Vision

...

## Mission

...

## Team Members

The following people are permanent members of the [Blank] Team:

<%= direct_team(manager_role: 'Engineering Manager, [Blank]') %>

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[,&] Blank/, direct_manager_role: 'Engineering Manager, [Blank]') %>

## Common Links

 * Issue Tracker
 * Slack Channel
 * ...

 ## How to work with us

 ...