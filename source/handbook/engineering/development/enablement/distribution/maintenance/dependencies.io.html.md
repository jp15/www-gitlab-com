---
layout: markdown_page
title: "Distribution Team usage of dependencies.io"
---

## On this page

- TOC
{:toc}

## Common links

* [dependencies.io](https://dependencies.io)
* [depenencies.io docs](https://www.dependencies.io/docs/)

## Introduction

We use dependencies.io to monitor external repositories, and open a merge request to the appropriate project when an upgrade is release.

The projects currently using this are

* [omnibus-gitlab](https://gitlab.com/gitlab-org/omnibus-gitlab)
  * [Open Merge Requests](https://gitlab.com/gitlab-org/omnibus-gitlab/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=dependencies.io&label_name%5B%5D=maintenance)
* [CNG images](https://gitlab.com/gitlab-org/build/CNG)
  * [Open Merge Requests](https://gitlab.com/gitlab-org/build/CNG/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=dependencies.io&label_name[]=maintenance)

Each repository contains at least two files

* dependencies.yml - This is the base configuration for dependencies.io.
* dependencies_io/git_repos.yml - The configuration for the software components we're monitoring using the [git-repos](https://www.dependencies.io/docs/dependencies/git-repos/) component.

## Handling Merge Requests

When a merge request for the project is opened, it should automatically assign to `ibaum`, and
mention the Distribution team. The pipelines are configured to automatically build and test (where appropriate)
a project using the new software.

It is the responsibility of the entire Distribution team to ensure the merge requests
are handled in a timely fashion. Team members should assign available merge
requests to themselves when they are going to work on them. The team member needs
to determine the appropriate milestone to target for the upgrade, and verify the
new software version works as expected. If everything looks good, a CHANGELOG
entry should be added, and the merge request assigned to a maintainer.

### omnibus-gitlab

The pipeline for the merge requests should run a triggered pipeline, which will build a package, and run gitlab-qa
against the package. Depending on the software, manual testing may be required. Once satisfied, a CHANGELOG entry
should be made, and the merge request should be assigned to a maintainer.

### CNG

The pipeline will build a new set of images using the required software. An instance of the helm charts should be
started, and testing done against that instance. Once complete, the MR should be assigned to a maintainer for
merging.
