---
layout: markdown_page
title: "GitLab Communication"
extra_js:
  - libs/moment.min.js
  - libs/moment-timezone-with-data.min.js
  - team-call.js
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

We're an [all-remote](https://about.gitlab.com/company/culture/all-remote/#advantages-for-organizations) company that allows people to work from [almost anywhere in the world](https://about.gitlab.com/jobs/faq/#country-hiring-guidelines). We hire great people regardless of where they live, but with GitLabbers across 52 countries (and counting) it's important for us to practice clear communication that helps us stay connected and get things done.
For this, we use **asynchronous communication as a start** and are as open as we can be by communicating through public issues, [Slack channels](/handbook/communication/chat),
and placing an emphasis on ensuring that conclusions of offline conversations are written down.
When we go **back and forth three times** we jump on a [synchronous video call](https://about.gitlab.com/handbook/communication/#video-calls).
These communication guidelines are meant to facilitate smooth communication in an ever-growing, all-remote company.

## External communication

Please see our [social media guidelines](/handbook/marketing/social-media-guidelines/).

## Everything starts with an issue

1. Always **create** an issue for things you work on. If it is worth spending time on, it is worth creating an issue since that enables other people to learn and help. You can always edit the description or close it when the problem changed to something different or was solved.
1. Issues, by default, should be **non-confidential**. Exceptions to this are security issues, issues specific to the operating of GitLab Inc (such as [contracts](https://about.gitlab.com/handbook/signing-legal-documents/) and [approving and paying invoices](https://about.gitlab.com/handbook/finance/procure-to-pay/)), issues that would violate confidentiality for a GitLab employee or customer, and issues that reveal revenue (such as the specific [IACV](https://about.gitlab.com/handbook/finance/operating-metrics/#incremental-annual-contract-value-iacv) of an opportunity) or financial data (such as the total monthly cash inflow/outflow for GitLab.com). When possible, consider sanitizing sensitive information from a confidential issue and making it non-confidential so the wider community can contribute.
1. Not every solution will solve the problem at hand. Keep discussions focused by **defining the problem first**.
1. Be proactive and consistent with communication on issues that have external stakeholders such as customers. It's important to keep communication flowing to keep everyone up to date. Issues can appear stale if there aren't recent updates and no clear definition on when another update will be provided, even though there may be complex development happening behind the scenes, or feedback pending from a third-party. This leaves those subscribed in the dark, causing unnecessary surprise if something ends up delayed and suddenly jumps to the next milestone. 
  - One way of doing this is having the current assignee set a due date for when they will provide another update. This can be days or weeks ahead depending on the situation, prioritization, and available capacity that we may have.
1. If a user suggests an enhancement, try and find an existing issue that addresses their concern, or create a new one. Ask if they'd like to elaborate on their idea in one of these issues.
1. **Double link** issues with related conversations. E.g. if there’s a ZenDesk ticket that causes you to open a GitLab.com issue, make sure to document the issue link in the ZenDesk ticket and vice versa. And when closing the issue, include reason or response from ZenDesk. Another example is to add "Report: " lines to the issue description with links to relevant issues and feature requests. When done add a comment to relevant issues (and close them if you are responsible for reporting back, or re-assign if you are not). This prevents internal confusion and us failing to report back to the reporters.
1. If two issues are related, **crosslink** them (a link from each issue to the other one). Put the link at the top of each issue's description with a short mention of the relationship (Report, etc.). If there are more than 2 issues, use one issue as the central one and crosslink all issues to this one. Please, also crosslink between ZenDesk and GitLab issues.
1. After a discussion about a feature **update the issue description** with the consensus or final conclusions. This makes it much easier to see the current state of an issue for everyone involved in the implementation and prevents confusion and discussion later on.
1. Submit the **smallest** item of work that makes sense. When creating an issue describe the smallest fix possible, put suggestions for enhancements in separate issues and link them. If you're new to GitLab and are writing documentation or instructions, submit your first merge request for at most 20 lines.
1. Do not leave issues open for a long time, issues should be **actionable** and realistic. If you are assigned to an issue but don't have time to work on it, assign it to someone else.
1. Make a conscious effort to **prioritize** your work. The priority of items depends on multiple factors: Is someone waiting for the answer? What is the impact if you delay it? How many people does it affect, etc.? This is detailed in [Engineering Workflow](/handbook/engineering/workflow).
1. Use the public issue trackers on GitLab.com for everything since [we work out in the open](/2015/08/03/almost-everything-we-do-is-now-open/). Issue trackers that can be found on the relevant page in the handbook and in the projects under [the gitlab-com group](https://gitlab.com/gitlab-com/).
1. Pick issues from the current [milestone](https://gitlab.com/groups/gitlab-org/milestones).
1. Avoid assigning issues to another person, unless you are their direct manager. People should preferably **pick issues** in a milestone themselves.
1. Assign an issue to yourself as soon as you start to work on it, but not before that time. If you complete part of an issue and need someone else to take the next step, **re-assign** the issue to that person.
1. When re-assigning an issue, make sure that the issue description contains the latest information. The issue description should be the **single source of truth**.
1. If you want someone to review an issue, do not assign them to it. Instead, @-mention them in an issue comment. Being assigned to an issue is a signal that the assignee should or intends to work on it. So you should not assign someone to an issue and mis-represent this with a false signal.
1. When working on an issue, **ask for feedback** from your peers. For example, if you're a designer and you propose a design, ping a fellow designer to review your work. If they approve, you can move it to the next step. If they suggest changes, you get the opportunity to improve your design. This promotes collaboration and advances everyone's skills.
1. Respond to comments within a **threaded discussion**. If there isn't a discussion thread yet, you can use the [Reply to comment](https://docs.gitlab.com/ee/user/discussions/#start-a-discussion-by-replying-to-a-standard-comment) button from the comment to create one. This will prevent issues from containing many interweaved discussions with responses that are hard to follow.
1. We keep our **promises** and do not make external promises without internal agreement.
1. Do not close an issue until it is [**done**](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#definition-of-done).
1. When **closing** an issue leave a comment explaining why you are closing the issue.

## Implement it with a merge request

Merge request guidelines for all contributors are described in our [Contribution
guide](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#merge-request-guidelines).

Code review guidelines for reviewers and maintainers are described in our [Code
Review Guidelines][code-review-guidelines].

Following are additional guidelines for GitLab Inc. team
members:

1. Even when something is not done, share it internally so people can comment
  early and prevent rework. Create a **[Work In
  Progress](/2016/01/08/feature-highlight-wip/)** merge request so it is not merged by accident.
1. If any followup actions are required on the issue after the merge request is
  merged (like reporting back to any customers or writing documentation), avoid
  auto closing it.
1. When _you_ are done with your merge request, remove the "WIP" prefix and
  follow the [Code Review Guidelines][code-review-guidelines].
1. You can still make changes based on feedback of course, but by removing the
  "WIP" prefix it clarifies that the main body of work has been completed.

[code-review-guidelines]: https://docs.gitlab.com/ee/development/code_review.html

## Internal Communication

1. All written communication happens in English, even when sent one on one, because sometimes you need to forward an email or chat.
1. Use **asynchronous communication** when possible (issues/email/Slack).
1. Issues are preferred over email, email is preferred over Slack, announcements happen on the company call agenda, and [people should be able to do their work without getting interrupted by chat](https://m.signalvnoise.com/is-group-chat-making-you-sweat-744659addf7d#.21t7089jk).
1. To use email instead of chat it is OK to send an _internal_ email that contains only a short message, similar as you would use in chat. Save time by not including a salutation like 'Hi Emma,' and first write the subject of the email which you copy and paste into the body. You are not expected to be available all the time. It is perfectly fine not to respond to emails and chat mentions until your planned work hours.
1. Sometimes synchronous communication is the better option, but do not default to it. For example, a video call can clear things up quickly when you are blocked. See the [guidelines on video chats](#video-calls) for more detail.
1. It is very OK to ask as many questions as you have. Please ask them so many people can answer them and many people see the answer, so use issues or public chat channels (like #questions) instead of private messages or one-on-one emails. If someone sends you a handbook link they are proud that we have the answer documented, they don't mean that you should have found that yourself or that the this is the complete answer, feel free to ask for clarification. If the answer to a question isn't documented yet please immediately make a merge request to add it to the handbook in a place you have looked for it. It is great for the person who answered the question to see you help to ensure they have to answer it only once. A merge request is the best way to say thanks for help.
1. If you mention something (a merge request, issue, commit, webpage, comment, etc.) please include a link to it.
1. All company data should be **shareable** by default. Don't use a local text file but rather leave comments on an issue.
1. When someone asks something, give back a deadline or that you did it. Answers like: 'will do', 'OK', 'it is on my todo list' are not helpful. If it is small is better to spend 2 minutes and do the tasks so the other person can mentally forget about it. If it is large you need to figure out when you'll do it, by returning that information the other person might decide to solve it in another way if it takes too long.
1. It is OK to bring an issue to someone's attention with a CC ("cc @user"), but CCs alone are not enough if specific action is needed from someone. The mentioned user may read the issue and take no further action. If you need something, please explicitly communicate your need along with @ mentioning who you need it from.
1. Avoid creating private groups for internal discussions:
    1. It's disturbing (all user in the group get notified for each message).
    1. It's not searchable.
    1. It's not shareable: there is no way to add people in the group (and this often leads to multiple groups creation).
    1. They don't have a subject, so everyone has to remember the topic of each private group based on the participants, or open the group again to read the content.
    1. History is lost when leaving the group.
1. It is perfectly fine to create a channel, even for a single customer meeting. These channels should be named "a_<customer-name>-internal" to indicate their "internal" nature (not shared with customers).
1. Use [low-context communications](https://en.wikipedia.org/wiki/High-context_and_low-context_cultures) by being explicit in your communications. We are a remote-only company, located all over the world. Provide as much context as possible to avoid confusion. For example, consider introducing yourself and your function when addressing the company during the company call, since not everyone may know who you are. Relatedly, we use [ubiquitous language](#ubiquitous-language) for communication efficiency.

### Asking "is this known"

1. If something is behaving strangely on [https://gitlab.com](https://gitlab.com), it might be a bug.
   It could also mean that something was changed intentionally.
1. Please search if the issue has already [been reported][issue-list].
    1. If it has not been reported, please file an issue, because [Everything starts with an issue][everything-issue]
1. If you are unsure whether the behavior you experience is a bug, you may ask in the Slack channel [#is-this-known][is-this-known-slack].
    1. Make sure that no-one has experienced this issue before, by checking the channel for previous messages
    1. If you know which stage of the DevOps lifecycle is affected, it is also okay to ask in #g_{group}, for example [#g_monitor][g_monitor-slack].
    1. Describe the behavior you are experiencing, this makes it searchable and easier to understand.
       Different people might look for different things in the same screenshots.
    1. Asking in a single channel helps discoverability, duplicated efforts and reduces noise in other channels.
       Please refrain from asking in general purpose channels like [#frontend][fe-slack], [#backend][be-slack], [#development][dev-slack] or [#questions][q-slack].

[issue-list]: https://gitlab.com/groups/gitlab-org/-/issues
[everything-issue]: /handbook/communication/#everything-starts-with-an-issue
[is-this-known-slack]: https://gitlab.slack.com/messages/CETG54GQ0/
[g_monitor-slack]: https://gitlab.slack.com/messages/C1ZCCRWBC
[fe-slack]: https://gitlab.slack.com/messages/C0GQHHPGW/
[be-slack]: https://gitlab.slack.com/messages/C8HG8D9MY/
[dev-slack]: https://gitlab.slack.com/messages/C02PF508L/
[q-slack]: https://gitlab.slack.com/messages/C0AR2KW4B/

## Email

1. Send one email per subject as multiple items in one email will cause delays (have to respond to everything) or misses (forgot one of the items).
1. Always reply to emails by replying to all, even when no action is needed. This lets the other person know that you received it. A thread is done when there is a single word reply, such as OK, thanks, or done.
1. If you forward an email without other comments please add FYI (for your information), FYA (for your action), or FYC (for your consideration). If you forward an external request with FYC it just means the person who forwarded it will not follow up on the request and expects you to decide if you should follow up or not, the terms comes from [movie promotion to voters](https://en.wikipedia.org/wiki/For_Your_Consideration_(advertising)).
1. Email forwarding rules are specified in the shared [_Email, Slack, and GitLab Groups and Aliases_](https://docs.google.com/document/d/1rrVgD2QVixuAf07roYws7Z8rUF2gJKrrmTE4Z3vtENo/) Google Doc accessible only to people in the company. If you want to be added or removed from an internal alias, change a rule, or add a forwarding email alias, please [suggest an edit](https://support.google.com/docs/answer/6033474?hl=en) in the doc and [submit a new access request](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=New%20Access%20Request).
1. Only G-Suite domain admins are allowed to provision Google Groups and email distributions.
1. Emails are asynchronous, for example, if your manager emails you on a weekend it is fine to reply during the workweek.
1. If an email is or has become **urgent** feel free to ping people via chat referencing the subject of the email.

## Slack

1. 1. Slack is to be used for informal communication only.  Only 90 days of activity will be retained.
1. If you use Slack, please avoid private messages. [Private messages discourage collaboration](http://blog.flowdock.com/2014/04/30/beware-of-private-conversations/). You might actually be contacting the wrong person and they cannot easily redirect you to the right person. If the person is unavailable at the moment, it is less efficient because other people cannot jump in and help. Use a public channel and mention the person or group you want to reach. This ensures it is easy for other people to chime in, involve other people if needed, and learn from whatever is discussed.
1. If you use Slack and plan to message 3 or more people, we recommend a channel for customer/issue/project/problem/partnership.
1. If you're only referring to someone, but don't actually need their attention, and want to spare them from getting notified, spell out their name normally without `@` mentioning them.
1. If the subject is of value to the wider community, consider commenting on an existing issue or opening a new [issue](#everything-starts-with-an-issue) instead.
1. Slack messages should be considered asynchronous communication and you should not expect an instantaneous response; you have no idea what the other person is doing.
1. Use the `:white_check_mark:` emoji or similar to indicate an inquiry has been answered. Anyone can add the emoji. If you're not sure, then feel free to leave it up to the person who asked. An emoji indicator is particularly helpful in channels where lots of questions are posted, such as `#questions`, and `#git-help`.
1. If you must send a private message, don't start a conversation with "Hi" or "Hey" as that interrupts their work without communicating anything. If you have a quick question, just ask the question directly and the person will respond asynchronously. If you truly need to have a synchronous communication, then start by asking for that explicitly, while mentioning the subject. e.g. "I'm having trouble understanding issue #x, can we talk about it quickly?".
1. Unless you're in an active chat, don't break up a topic into multiple messages as each one will result in a notification which can be disruptive. Use [threads](https://get.slack.help/hc/en-us/articles/115000769927-Message-threads) if you want to provide extra info to the question/comment you posted. In channels like `#questions`, threads are valuable for keeping conversations together.
1. Because we work globally, you may receive Slack mentions at any time of day. Please consider enabling [Slack's Do not disturb functionality](/handbook/tools-and-tips/#do-not-disturb-hours) so you don't get interrupted, for example, in your offtime.
1. Do not feel obligated to respond to Slack messages when you are not working.
1. Feel free to send a colleague a link to these guidelines if the communication in Slack should be done **asynchronously**.
1. If you are having a hard time keeping up with messages, you can update your preferences to have Slack email you all notifications. To change the setting, go to `Preferences > Notifications > When I'm not active on desktop...` and "send me email notifications".
1. **Please avoid using @here or @channel unless this is about something urgent and important.** In chat try to keep the use of keywords that mention the whole channel to a minimum. They should only be used for pings that are both urgent and important, not just important. By overusing channel mentions you make it harder to respond to personal mentions in a timely manner since people get pinged too frequently. If something is urgent and important:
   * Use `@here` to notify all currently _active_ members in the room.  Please only use `@here` if the message is important _and_ urgent.
   * Use `@channel` to notify _ALL_ members in the room, irrespective of away status. Please only use `@here` if the message is important _and_ urgent.
1. If something is important but not urgent - like complimenting or encouraging the entire team - use email or post in the channel without `@`-mentioning the team.
1. If you agree in a message to start a video call (typically by asking "Call?") the person that didn't leave the last comment starts the call. So either respond to the "Call?" request with a video link or say "Yes" and let the other person start it. Don't say "Yes" and start a call 5 seconds later since it is likely you'll both be creating a video call link at the same time.
1. The usage of ChatBots for integrations can sometimes depend upon the name of the channel. You should consult the channel about such integrations before changing the name of commonly used / popular channels in order to avoid inadvertently breaking integrations.
1. If you are aware that your teammate is on vacation, avoid mentioning them in a high volume channel. It will be difficult to find the information or question when they return. If you need to ensure they refer back to the thread, ensure to send them a link to the relevant Slack message through a direct message.
1. It's not rude to leave a channel. When you've had your questions answered or are no longer interested, feel free to leave the channel so it won't distract you anymore.
1. As an admin of the Slack workspace, if given the option to _"Disable future attachments from this website"_ when removing an attachment from a message **this will blacklist the link/domain from [unfurling](/handbook/tools-and-tips/#unfurling-links-in-messages) in the entire Slack workspace**.  Be careful and deliberate when chosing this option as it will impact every user in the workspace.
1. Learn about [common channels and channel-naming conventions](/handbook/communication/chat).

### Emergency Chat
#### Slack is down
{:.no_toc}
   1. Use the "Slack Down!" group chat on Zoom.
      1. In the Zoom desktop app go to the *Contacts* tab
      1. Click `+`
      1. Click "Join a Channel"
      1. Search "Slack down!"
      1. Click "Join"

#### Slack and Zoom are down
{:.no_toc}
   1. Join the [Slack Down!](https://chat.google.com/preview/room/AAAAGAd_BaQ) room on Hangouts Chat

**IMPORTANT**: Once service is restored, go back to Slack.

## Google Docs

1. Never use a Google Doc / Presentations for something non-confidential that has to end up on the website or the **handbook**. Work on these edits via commits to a merge request. Then link to the merge request or diff to present the change to people. This prevents a duplication of effort and/or an out of date handbook.
1. If you _do_ need a Google Doc, create one with your company G Suite (formerly Google
Apps) account. By default, share it with the whole company using the _Anyone at GitLab can find and access_ link sharing permission
and the _Anyone within GitLab can edit_ access permission (preferred) or the _Anyone within GitLab can comment_ access permission.
Easily do this by creating Google Docs within a Shared Folder in Google Drive.
1. If you have content in a Google doc that is later moved to the website or handbook, [deprecate the Google doc](#how-to-deprecate-a-google-doc).
1. When referring to a Google Doc or folder on the Google Drive in the handbook, refrain from directly linking it. Instead, indicate the name of the doc. If you link the url people from outside the organization can request access, creating workload and the potential for mistakes.
(In the past, linking a Google Doc has led to inadvertently opening the sharing settings beyond what was intended.)
This also helps prevent spam from people outside GitLab requesting access to a doc when clicking its link.
1. If you are having trouble finding a shared Google Doc, make sure you [Search &lt;your domain&gt;](https://support.google.com/a/answer/3187967?hl=en) in Google Drive.
1. In our handbook, if you find yourself wondering whether it is better to provide a public link to a Google Doc vs. writing out the content on the website, use the following guideline: Is this document frequently adapted / customized? If yes, then provide a link, making sure that the document can be _commented on_ by _anyone_ with the link. For instance, this is how we share our employment [contracts](/handbook/contracts/). If the document is rarely customized, then provide the content directly on the site and deprecate the Google Doc.
1. If you want to quickly find where a team member's cursor is in a Google Doc, click their icon at the top of the document and the app will jump you to the current location. This works in Sheets and Presentations as well.
1. You can set the notifications on a google doc so you only get emailed with someone tags you directly instead of getting emails for every comment. Click on "notifications" and select "Only yours". By the way, when you create the doc, it defaults to All, but when you are just shared with it, it defaults to Only yours. There is [no global default](https://productforums.google.com/forum/#!msg/docs/1C3PZX1AY7Q/6EXXQKQSPCMJ).
![Google Doc Notifications](./google-docs-notifications.png)

### How to deprecate a Google doc

1. Add 'Deprecated: ' to the start of the title.
1. Remove the content you moved.
1. Add a link to the new location at the beginning of the doc/first slide/first tab.
1. Add a link to the merge request or commit that moved it (if applicable).

## Presentations

1. All presentations are made in Google Slides using [our templates](/handbook/tools-and-tips/#google-slides-templates).
1. Please allow anyone at GitLab to edit the presentation (preferred) or at least comment on the presentation.
1. If the content can be public use File > Publish to the web > Publish to get a url and paste that in the speaker notes of the first slide (commonly the title slide).
1. The title of every slide should be the message you want the audience to take away, not the subject matter. So use 'Our revenue more than doubled' instead of 'Revenue growth'.
1. At the end of the presentation when you go to Q&A stop presenting in Zoom. This way the other people can see the person who is speaking much better.

## Say Thanks

1. Thank people that did a great job in our "Thanks" Slack channel.
1. If someone is a team member just @ mention them, if multiple people were working on something try @ mentioning each person.
1. If possible please include a link with your thanks that points to the subject matter that you are giving thanks for, for example a link to a merge request.
1. Add emoji reactions to other peoples thank you messages. Use the value emojis when you see a thank you message ties to [GitLab's values](https://about.gitlab.com/handbook/values/).
  - `:collaboration-value: :results-value: :efficiency-value: :diversity-value: :iteration-value: :transparency-value:`
  - ![CREDIT emoji](/images/handbook/credit-emoji.png)
1. Please do not mention working outside of working hours, we want to minimize the pressure to do so.
1. Almost [everyone in the company is active in this channel](https://twitter.com/sytses/status/1100071442576633856) so please don't be shy.
1. Don't thank the CEO or other executives for something that the company paid for, thank GitLab instead.
1. To thank someone who is not a team member please mention a People Ops Coordinator, the name of the person, a quirky gift
and link to their work. For example, "@manager, @peopleopscoordinator: Joe deserves a lawnmower for _link_". The People Ops Coordinator will approach the person in question for their address saying we want to send some swag. We'll ship it in gift wrap with "Thanks for your great work on _link_, love
from @gitlab".

## Not sure where to go?

If there is something that you want to discuss, but you do not feel that it is
a reasonable option to discuss with either your manager or CEO, then you can reach
out to any of the other C-level GitLabbers or our board member Bruce Armstrong.

## Company Call

1. Company calls happen Monday to Thursday.
1. Every Friday we have an AMA with an executive to talk about anything the people at our company are thinking about.
1. Schedule
   * PST: <span id="main-PST"></span>
   * UTC: <span id="main-UTC"></span>
   * <span id="main-abbr"></span>: <span id="main-USER"></span>
1. APAC schedule
   * PST: <span id="apac-PST"></span>
   * UTC: <span id="apac-UTC"></span>
   * <span id="apac-abbr"></span>: <span id="apac-USER"></span>
1. Everyone at GitLab is invited to the company call.
1. We also have a company call for GitLabbers in the APAC region. This call will also be recorded so the rest of the company can see what their colleagues have been up to! Everyone is encouraged to join this call as well, but it is not mandatory.
1. We use [Zoom](https://gitlab.zoom.us) for the call since Google Hangouts is capped at 15 people (please be sure to mute your microphone). If using the Zoom web app, you can go to settings and check always mute microphone when joining a meeting.
1. The link is in the calendar invite and also listed at the top of the company call agenda Google Doc called _Company Call Agenda_.
1. The call is recorded automatically, and all calls are transferred every hour to a Google Drive folder called "GitLab Videos". There is a subfolder called "GitLab Company Call", which is accessible to all users with a GitLab.com e-mail account.
1. We start on time and do not wait for people.
1. The person who has the first item on the agenda starts the call.
1. We start by discussing the subjects that are on the agenda for today.
   * Everyone is free to add subjects. Please start with your name and be sure to link to an issue, merge request, or commit if it is relevant.
   * When done with a point mention the subject of the next item and hand over to the next person.
   * When someone passes the call to you, no need to say, “Can you hear me?” Just begin talking. If we can’t hear you, we’ll let you know.
1. After the general announcements we do a [breakout call](#breakout-call).
1. It is OK to talk over people or interrupt people to ask questions, cheer for them or show your compassion. This encourages more conversation and feedback in the call. Also see the interruption item in [video calls](#video-calls).
1. Please look if the person you hand over to is present in the participant list so you don't hand over to someone who is not present.
1. The last person on the agenda transfers to the group calls.
1. Even if you cannot join the call, consider reviewing the recorded call or at minimum read through the agenda and the links from there. We often use the company call to make announcements or discuss changes in processes, so make sure to catch up on the news if you have missed a company call (or more).
1. If you are scheduling a meeting, avoid scheduling during the company call so that meeting attendees do not need to choose between your meeting and the company call. As a remote workforce, the company call is an important part of our culture.

### Breakout call

1. We split into small groups of at the end of the [Company Call](#company-call) mostly after 5 minutes into the call.
1. Every call has a theme, but you can talk about anything, as long as it isn't about results at work.
1. [Breakout Groups](https://docs.google.com/spreadsheets/d/1M-6NjK8_gAqcU1BehQVkKuDa1Ua2hTGBy8TuEDXHh3g/edit?usp=sharing)
1. Every quarter half of each group is combined with another group to allow you to get to know new people without losing all the people you're familiar with.
1. The consistent group allows you to build a deeper relation with a group of people all over the company. We had daily random groups before but that didn't work. As someone said since switching to stable groups: "I really like the breakout discussion format with the constant group of attendees we have now.  It’s good to get some repeat conversations to get to know each other better.  I find the conversation is going faster and is more engaging as we add a little context with the group each time.  This is a change for me as I was not getting nearly as much out of the breakouts in the old format."

### Call Structure

These calls are unstructured to allow for natural interaction, but everyone should get a chance to share at least once.

It helps to pause slightly between talking about different topics to allow for discussion.

If you are unsure of who should start or speak next, follow the order listed in Zoom.

Prompts for things you might share:

- Hobbies and interests.
- What you've been up to recently.
- Things you're looking forward to.
- Sports and wellness.
- Cooking, entertaining, and creative projects.
- Travel, kids, family, and pets.
- Music, books, TV & movies, and video/board games.

### Setting up the calls

The calls take place using the People Ops zoom account, which has Breakout Rooms enabled. Make sure to be logged in to the People Ops account to host the calls.

Join the Group Conversation 5 minutes prior to the start time to make sure everything is working properly, and to assist the presenter with any needs. If for some reason the call does not automatically begin recording, press the record button in the bottom right of the Zoom window.  Observe over the call and mute any users that are unmuted on accident by clicking the mute button over their image. End the call for all participants at the end of the discussion.  If the call runs long, and it is at the 29 minute mark, alert the presenter and end the call.

Next, join the company call.  Double check that the call is recording, and that participants can enter the room.  If there is any issues in joining the room, double check that the Group Conversation call was ended properly.  Once the call begins, make sure to mute any unmuted users that aren't talking, or inform them so they can mute themselves.  After the company updates on Tues-Thurs see the following instructions:

##### Mixer Calls

After the announcements, the host will inform the call participants that they will be heading to breakout rooms, and give a suggestion discussion question or topic for the call. The host will [start the Breakout Rooms](https://support.zoom.us/hc/en-us/articles/206476313-Managing-Video-Breakout-Rooms) from the Zoom interface, choosing a number of rooms that averages 8-9 people per room.

Anyone who joins late or accidentally leaves their Breakout can be manually assigned to a room.

The host of the call will join the room with the least amount of participants.

At the end of the call, at 8:59 PDT, the host will hit the 'return users to main session button' and then end the call for all.

##### Company Call Groups

After the announcements have ended, the host will inform the group that they will be heading to their breakout rooms.  Inform the group to join their [predesignated groups](https://docs.google.com/spreadsheets/d/1M-6NjK8_gAqcU1BehQVkKuDa1Ua2hTGBy8TuEDXHh3g/edit?usp=sharing) in the chat, and let the team know to inform you (the host) if they are not on the list, and to join another room if they find their room to be empty.

If a team member is not on the listing, inform them to go to another room, and inform the host to add them to the most fitting room.

#### Archiving the Company Call Agenda

The company call agenda document will begin to  perform poorly (e.g. slow load times, freezing, restricted write access) as it grows. To curb that, events should be [archived](https://docs.google.com/document/d/1-X_oRGICNiOVUw8asuNP90tg6l6EjRVNSDvXoZAVeR8/edit) regularly. Events older than two months may be moved to the archive, which follows the same basic structure as the company call agenda document with most recent events appering towards the top of the doc.

A link to the archive can be found at the top of the company call agenda document.


## Release Retrospectives and Kickoffs
{: #kickoffs}

After GitLab releases a new version on the 22nd of each month, we have a
30-minute call a few days later reflecting on what could have been
better:

1. What went well this month?
1. What went wrong this month?
1. What could we have done better?

We spend the first part of the retrospective meeting reviewing the action
items from the previous month.

On the 8th of each month (or the next business day), we have a kickoff meeting
for the version that will be released in the following month. The product team and other leads will have already had
discussions on what should be prioritized for that release. The purpose of this kickoff is
to get everyone on the same page and to invite comments.

Both the retrospectives and kickoffs are [live streamed](/handbook/communication/youtube/#live-streaming)
(and posted) to the [GitLab YouTube channel](https://www.youtube.com/gitlab).

## Random
{: #random-room}

1. The [#random](https://gitlab.slack.com/archives/random) Slack channel is your go-to place to share random ideas, pictures, articles, and more. It's a great channel to check out when you need a mental break.
1. Come hang out any time in the **random room**, an open Google Hangout video chat where anyone with a GitLab email address can come and leave as they please. The link is in the description of the `#random` Slack channel; consider bookmarking it. This room is open for all and should be distinct from the [Coffee Break Calls](/company/culture/all-remote/#coffee-break-calls), which are 1x1 conversations between teammates.

## Scheduling Meetings

1. If you want to ask GitLabbers if they are available for an event please send a calendar invite with Google Calendar with your Google GitLab account to their Google GitLab account. This makes it easier for people to check availability and to put on their calendar. It automatically shows up on calendars even when the email is not opened. It is easier to signal presence and to see the status of everyone. Please respond quickly to invites so people can make plans.
1. Every scheduled meeting should either have a Google Presentation (for example for functional updates that don't require participation) or a Google Doc (for most meetings) linked. If it is a Google Doc it should have an agenda, including any preparation materials (can be a presentation). Put the agenda in a Google Doc that has edits rights for all participants (including people not part of GitLab Inc.). Link the Google Doc from the meeting invite. Take notes of the points and todos during the meeting. Nobody wants to write up a meeting after the fact and this helps to structure the thought process and everyone can contribute. Being able to structure conclusions and follow up actions in realtime makes a video call more effective than an in-person meeting. If it is important enough to schedule a meeting it is important enough to have a Doc linked. If we want to be on the same page we should be looking at that page.
1. If you want to check if a team member is available for an outside meeting, create a calendar appointment and invite the team member only after they respond yes. Then invite outside people.
1. When scheduling a call with multiple people, invite them using a Google Calendar that is your own, or one specific to the people joining, so the calendar item
doesn't unnecessarily appear on other people's calendars.
1. If you want to move a meeting just move the calendar appointment instead of reaching out via other channels. Note the change at the top of the description.
1. Please click 'Guests can modify event' so people can update the time in the calendar instead of having to reach out via other channels. You can configure this to be checked by default under [Event Settings](https://calendar.google.com/calendar/r/settings).)
1. When scheduling a meeting we value people's time and prefer the "speedy meetings" setting in our Google Calendar. This gives us meetings of, for example, 25 or 50  minutes leaving some time to write notes etc before continuing to our next call or meeting. (This setting can be found under the calendar Settings menu at "default event duration").
1. When scheduling a meeting, please try to have it start at :00 (hour) or :30 (mid-hour) to leave common start times available for other meetings on your attendees' calendars. Meetings should be for the time needed, so if you need 15 minutes just book that.
1. When creating a calendar event that will be used company wide, please place it on the GitLab Team Meetings Calendar. That way the event is easily located by all individuals.
1. When you need to cancel a meeting, make sure to delete/decline the meeting and choose the option **Delete & update guests** to make sure everyone knows you can't attend and don't wait for you.
1. If you want to schedule a meeting with a person not on the team please use [Calendly](/handbook/tools-and-tips/#calendly). Use Google Calendar directly if scheduling with a GitLabber.

## Indicating Availability

Indicate your availability by updating your own calendar using Google's ["out of office"](https://www.theverge.com/2018/6/27/17510656/google-calendar-out-of-office-option) feature and include the dates you plan to be away in your automated response. Note that this feature will automatically decline any meeting invitations during the time frame you select.

1. Put your planned away time including holidays, vacation, travel time, and other leave in your own calendar. Please see [Communicating your time off](/handbook/paid-time-off#communicating-your-time-off) for more.
1. Set your working hours in your Google Calendar settings.
1. Utilize [PTO Ninja](https://about.gitlab.com/handbook/paid-time-off/#pto-ninja) to keep other GitLabbers aware of your planned time away within Slack.

## Video Calls

1. Use video calls if you find yourself going back and forth in an issue/via email
or over chat. Rule of thumb: if you have gone **back and forth 3 times**, it's time
for a video call.
1. Sometimes it's better to _not_ have a video call. Consider these tradeoffs:
   1. It is difficult (or impossible) to multi-task in a video call.
   1. It may be more efficient to have an async conversation in an issue, depending on the topic.
   1. A video call is limited in time: A conversation in an issue can start or stop at any time, whenever there's interest. It is async.
   1. A video call is limited in people: You can invite anybody into an async conversation at any time in an issue. You don't have to know who are the relevant parties ahead of time. Everyone can contribute at any time. A video call is limited to invited attendees (and those who have accepted).
   1. You can easily "promote" an async conversation from an issue to a video call, as needed. The reverse is harder. So there is lower risk to start with an async conversation.
   1. For a newcomer to the conversation, it's easier and more efficient to parse an issue, than read a video transcript or watch it.
   1. Conversations in issues are easily searchable. Video calls are not.
1. Try to have your video on at all times because it's much more engaging for participants
   1. Don't worry if you can't pay attention at the meeting because you're doing something else, you are the manager of your attention
   1. It's okay to eat on video if you're hungry (please turn your mic off)
   1. However you are groomed when you meet a family member or close friend is fine for GitLab internal meetings (including if you just got back from a run)
   1. Having pets, children, significant others, friends, and family visible during video chats is encouraged. If they are human, ask them to wave at your remote team member to say "Hi".
   1. Do not feel forced to have your video on, use your best judgement
1. Additional points for video calls with customers or partners
   1. Results come first.  Your appearance, location and background is less important than making customers successful so don't wait for the perfect time / place when you can engage a customer right away.
   1. Communicating that GitLab is an enterprise grade product and service provider is supported by the way you present yourself.  Most of the time, if you would not wear something or present yourself in a certain way at a customer's office, candidate interview, or partner meeting in person then it's probably not the right choice on a video call with them either.
   1. Green screens are a great background solution.  It's great to work in your garage or basement!  Just get a green screen behind you and put up a professional background image to present well externally and still use the rest of the room how you want!
1. We prefer [Zoom](https://gitlab.zoom.us/).
1. For meetings that are scheduled via calendar there is automatically a Google Hangouts URL added. This is the meeting place. Having a URL in advance is much more reliable than trying to call via Hangouts as the meeting start.
1. Google Calendar also has a [Zoom plugin](https://chrome.google.com/webstore/detail/zoom-scheduler/kgjfgplpablkjnlkjmjdecgdpfankdle?hl=en-US) where you can easily add a Zoom link for a video call to the invite
1. For meetings that are scheduled with Zoom:
   1. Make sure to take out the Google Hangouts link to avoid confusion.
   1. If you need more privileges on Zoom (longer meeting times, more people in the meeting, etc.), please contact People Ops as described [specifically for Zoom](/handbook/tools-and-tips/#sts=Zoom).
   1. Note that if you select to record meetings to the cloud (setting within Zoom), they will be automatically placed in the GitLab Videos folder in Google Drive on an hourly basis. You can find these videos in Google Drive by entering in the search bar: `title:"GitLab Videos" source:domain`.
   1. Note also that after a meeting ends, Zoom may take some time to process the recording before it is actually available. The sync to Google Drive happens on the hour mark, so if the recording is not available, it may take another hour to be transferred.
1. Use a headset with a microphone, [Apple EarPods](https://www.apple.com/shop/accessories/all-accessories/headphones-speakers) are great. Do not use computer speakers, they cause an echo. Do not use your computer microphone, it accentuates background noise. If you want to use your [Bose headphones](https://www.bose.com/en_us/products/headphones/noise_cancelling_headphones.html) that is fine but please ensure the microphone is active.
1. Consider using a utility to easily mute/unmute yourself, see [Shush](/handbook/tools-and-tips/#shush) in the tools section.
1. [Hybrid calls are horrible](hybrid-calls-are-horrible)
1. Please speak up asap when someone on the call hasn't muted their mic to avoid disturbances to the person speaking.
1. We start on time and do not wait for people. People are expected to join no later than the scheduled minute of the meeting (before :01 if it is scheduled for :00). The question 'is everyone here' is not needed.
1. It feels rude in video calls to interrupt people. This is because the latency causes you to talk over the speaker for longer than during an in-person meeting. We should not be discouraged by this, the questions and context provided by interruptions are valuable. This is a situation where we have to do something counter-intuitive to make all-remote meetings work. In GitLab, everyone is encouraged to interrupt the speaker in a video call to ask a question or offer context. We want everyone to contribute instead of a monologue. Just like in-person meetings be cognizant of when, who, and how you interrupt, we don't want [manterrupting](http://time.com/3666135/sheryl-sandberg-talking-while-female-manterruptions/).
1. We end on the scheduled time. It might feel rude to end a meeting, but you're actually allowing all attendees to be on time for their next meeting.
1. Do not use the chat of products like Zoom to communicate during the call, use the linked document instead. This allows everyone to contribute additional questions, answers, and links in the relevant place. It also makes it easier for people in conflicting timezones to contribute questions before the call and makes it easier to review questions and answers after the call, which can be before watching the recording.
1. Every comment is document worthy, even small support comments such as `+1` or `Very Cool!`.

## Hybrid calls are horrible

In calls that have remote participants everyone should use have their own equipment (camera, headset, screen).

When multiple people share equipment the following **problems arise for remote participants**:

1. Can't hear the sharing people well.
1. Background noise since the microphone of the sharing people on all the time.
1. Can't clearly see facial expressions since each face takes up only a small part of the screen.
1. Can't easily see who is talking since the screen shows multiple people.
1. Hard  getting a word in since their delay is longer than for the sharing people.

The **people sharing equipment also have problems** because they don't have their own equipment:

1. Can't easily screen share something themselves.
1. Trouble seeing details in screen sharing since the screen is further away from them.
1. Can't scroll through a slide deck at their own pace.
1. Sharing people can't easily participate (view or type) in a shared document with the agenda and meeting notes.

The disadvantages for remote people are much greater than for the sharing people and hard to notice for the sharing people.
The disadvantages cause previously remote participants to travel to the meeting to be in person for a better experience.
The extra travel is inefficient since it is time consuming, expensive, bad for the environment, and unhealthy.

Theoretically you can have multiple people in a room with their own equipment but in practise it is much better to be in separate rooms:

1. It is annoying to first hear someone talk in the room and then hear it over audio with a delay.
1. It is hard to consistently mute yourself when not talking to prevent someone else their voice coming through your microphone as well.

## Google Calendar

We recommend you set your Google calendar access permissions to 'Make available for GitLab - See all event details'. Consider marking the following appointments as 'Private':

- Personal appointments.
- Confidential & sensitive meetings with third-parties outside of GitLab.
- 1-1 performance or evaluation meetings.
- Meetings on organizational changes.

There are several benefits and reasons to sharing your calendar with everyone at GitLab:

1. Transparency is one of our values and sharing what you work on is in line with our message of "be open about as many things as possible".
1. Due to our timezone differences, there are small windows of time where our availabilities overlap. If other members need to schedule a new meeting, seeing the details of recurring meetings (such as 1-1s) will allow for more flexibility in scheduling without needing to wait for a confirmation from the team member. This speaks to our value to be more efficient.

![Google Calendar - make calendar available setting](/images/handbook/tools-and-tips/google-calendar-share.png)

If you add blocks of time spent on recurring tasks to your Google Calendar to remind yourself to do things (e.g. "Check Google Analytics"), consider marking yourself "Free" for those events so that coworkers know they may schedule a meeting during that time if they can't find another convenient time.

## User Communication Guidelines

1. Keep conversations positive, friendly, real, and productive while adding value.
1. If you make a mistake, admit it. Be upfront and be quick with your correction. If you're posting to a blog, you may choose to modify an earlier post. Just make it clear that you have done so.
1. There can be a fine line between healthy debate and incendiary reaction. Try to frame what you write to invite differing points of view without inflaming others. You don’t need to respond to every criticism or barb. Be careful and considerate.
1. [Assume positive intent](/handbook/values/#assume-positive-intent) and explicitly state the strongest plausible interpretation of what someone says before your respond, not a weaker one that's easier to criticize.
1. Answer questions, thank people even if it’s just a few words. Make it a two way conversation.
1. Appreciate suggestions and feedback.
1. Don't make promises that you can't keep.
1. Guide users who ask for help or give a suggestion and share links. [Improving Open Development for Everyone](/2015/12/16/improving-open-development-for-everyone/), [Types of requests](/2014/12/08/explaining-gitlab-bugs/).
1. When facing negative comment, respond patiently and treat every user as an individual, people with the strongest opinions can turn into [the strongest supporters](/2015/05/20/gitlab-gitorious-free-software/).
1. Adhere to the [Code of Conduct](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#code-of-conduct) in all communication. Similarly, expect users to adhere to the same code when communicating with the GitLab team and the rest of the GitLab community. No one should accept being mistreated.

## Writing Style Guidelines

1. {: #american-english} At GitLab, we use American English as the standard written language.
1. Do not use rich text, it makes it hard to copy/paste. Use [Markdown](/handbook/product/technical-writing/markdown-guide) for things stored in git. In Google Docs use "Normal text" using the style/heading/formatting dropdown and paste without formatting.
1. Don't use ALL CAPS because if [feels like shouting](http://netiquette.wikia.com/wiki/Rule_number_2_-_Do_not_use_all_caps).
1. We use Unix style (lf) line endings, not Windows style (crlf), please ensure `*.md text eol=lf` is set in the repository's `.gitattributes` and run `git config --global core.autocrlf input` on your client.
1. Do not create links like "here" or "click here". All links should have relevant anchor text that describes what they link to, such as: "GitLab CI source installation documentation". Using [meaningful links](https://www.futurehosting.com/blog/links-should-have-meaningful-anchor-text-heres-why/){:rel="nofollow noindex"} is important to both search engine crawlers (SEO) and people with accessibility issues.
1. Always use [ISO dates](https://en.wikipedia.org/wiki/ISO_8601#Calendar_dates) in all writing and legal documents since other formats [lead to online confusion](http://xkcd.com/1179/). Use `yyyy-mm-dd`, for example 2015-04-13, and never 04-13-2015, 13-04-2015, 2015/04/13, nor April 13, 2015. Even if you use a unambiguous alternative format it is still harder to search for a date, sort on a date, and for other team members to know we use the iso standard. For months use `yyyy-mm`, so 2018-01 for January. Refer to a year with CY18 (never with 2018) and a quarter with CY18-Q1 to prevent confusion with fiscal years and quarters.
1. When referring to our quarterly and annual cadence, remember that our Fiscal Year runs from February 1 to January 31.  When referring to a fiscal year or quarter, please use the following abbreviations:
   1. "FY20" is the preferred format and means: Fiscal Year 2020, the period running from February 1, 2019 through January 31, 2020
   1. "Q1" = the first quarter of the current Fiscal Year, so on Feb 1, 2020, "Q1" is the period from Feb. 1, 2020 through April 30, 2020. Note that Epics in GitLab follow Calendar Years and Quarters. In Looker, Fiscal Years and Quarters are explicitly labeled while Calendar Years and Quarters are simply "Year" and "Quarter".
   1. When referring to a quarter in a future or past year, combine the two above: "FY21-Q1"
1. Remember that not everyone is working in the same timezone; what may be morning for you is evening for someone else. Try to say 3 hours ago or 4 hours from now, or use a timestamp, including a timezone reference.
1. We use UTC as the timezone for engineering (for example production postmortems) and all cross-functional activities related to the monthly release. We use Pacific Time (PT) for all other uses since we are a San Francisco-based company. Please refer to time as '9:00 Pacific' or `9:00 PT`. Don't use PDT or PST since it can be ambiguous depending on if it is currently daylight savings or not.
1. Although we're a San Francisco based company we're also an internationally diverse one. Please do not refer to team-members outside the US as international, instead use non-US. Please also avoid the use of offshore/overseas to refer to non-American continents.
1. If you have multiple points in a comment or email, please number them to make replies easier.
1. When you reference an issue, merge request, comment, commit, page, doc, etc. and you have the URL available please paste that in.
1. In making URLs, always prefer hyphens to underscores, and always use lowercase.
1. The community includes users, contributors, core team members, customers, people working for GitLab Inc., and friends of GitLab. If you want to refer to "people not working for GitLab Inc." just say that and don't use the word community. If you want to refer to people working for GitLab Inc. you can also use "the GitLab Inc. team" but don't use the "GitLab Inc. employees".
1. When we refer to the GitLab community excluding GitLabbers please say "wider community" instead of "community".
1. All people working for GitLab (the company) are the [GitLab team](/company/team). We also have the [Core team](/core-team) that consists of volunteers.
1. Please always refer to GitLab Inc. people as GitLabbers, not employees.
1. Use [inclusive and gender-neutral language](https://techwhirl.com/gender-neutral-technical-writing/) in all writing.
1. Always write GitLab with a capitalized G and L, even when writing GitLab.com.
1. Always capitalize the names of GitLab [features](/features).
1. Do not use a hyphen when writing the term "open source."
1. Monetary amounts shouldn't have one digit, so prefer $19.90 to $19.9.
1. If an email needs a response, write the answer at the top of it.
1. Use the future version of words, just like we don't write internet with a capital letter anymore. We write frontend and webhook without a hyphen or space.
1. Our homepage is / (with the `about.` and with `https`).
1. Try to use the [active voice](https://writing.wisc.edu/Handbook/CCS_activevoice.html) whenever possible.
1. Refer to environments that are installed and run "on-premises" by the end-user as "self-managed."
1. If you use headers, properly format them (`##` in Markdown, "Heading 2" in Google docs); start at the second header level because header level 1 is for titles. Do not end headers with a colon.
1. Always use an [Oxford comma](https://en.wikipedia.org/wiki/Serial_comma) in lists of three, four, or more terms.
1. Always use a single space between sentences rather than two.
1. Read our [Documentation Styleguide](https://docs.gitlab.com/ee/development/doc_styleguide.html) for more information when writing documentation.
1. Do not use acronyms when you can avoid it as you can't assume people know what you are talking about. Example: instead of `MR`, write `merge request`.
1. We segment our customers/prospects into 4 segments [Strategic, Large, Mid-Market, and Small Medium Business (SMB)](/handbook/business-ops/#segmentation).
1. When creating lists, use numbered lists instead of bullet lists, so points are easier to reference during a discussion.

## Situation-Complication-Implication-Position-Action-Benefit (SCIPAB)

Mandel Communications refers to SCIPAB at the "surefire, six-step method for starting any conversation or presentation."  When you only have a few minutes to present your case or grab your listeners attention, this six-step process can help you communicate better and faster.

* Situation - Expresses the current state for discussion.
* Complication - Summarizes the critical issues, challenges, or opportunities.
* Implication - Provides insight into the consequences that will be a result of if the Complications are not addressed.
* Position - Notes the presenter's opinion on the necessary changes which should be made.
* Action - Defines the expectations of the target audience/listeners.
* Benefit - Clearly concludes how the Position and Action sections will address the Complications.
This method can be used in presentations, emails, and everyday conversations.
Example - The Management team asking for time to resolve a problem
* S - The failure rate last year for product X1 was an acceptable 1.5%.
* C - Because of supply shortages in the current fiscal year we are forced to change the material of a key component.
* I - Unfortunately, that resulted in the failure rate doubling this year.
* P - It is critical we address this problem immediately.
* A - Please approve the team 5 days to investigate the specific causes of the increase and establish the necessary next steps.
* B - By doing this we will reduce the failure rate to an acceptable level and develop guidelines for preventing such problems in the future.
More information can be found at [SCIPAB- Six Steps To Reach Your Audience](https://dzone.com/articles/scipab-six-steps-to-reach-your-audience).

## Beamy Guidelines

Beamy is our company conference call robot. It lives in the San Francisco boardroom.
Its main purpose is to allow those outside of the boardroom a view into the space and people.
Practically speaking, in our case it serves two primary functions - helping prove that there
really is no central headquarters office full of employees on a regular basis, and secondly,
to show off to your kids, as a remote equivalent to bringing your kids in to work...

When you call in to the beam your face will appear on the screen and audio interaction will be
possible (so make sure your webcam and microphone are working first) and you can drive the
robot around the boardroom. It simulates being in the space without physically being there.
It is really cool and everyone should try it and have fun with it.

* You should have received an invite during onboarding to connect and to download a desktop and/or mobile client. If you did not receive the invite, please request one in the Slack #peopleops channel.
* **Beamy times**: 8am until 6pm Pacific time. [Please check the current time!](https://time.is/San_Francisco) on workdays and during all company events, for other times please @mention Sytse in the Slack #valley channel to see if it is OK.
* Your account should automatically log right in and give you immediate access to the Beamy.  If attempting to beam in results in a long wait and timeout, please let someone in the #peopleops channel know, as it is possible that your account is misconfigured and requires an answer on the other end.
* The email invite will come from "Suitable Tech". Once you are sent an invite you can beam in at any time and drive around. Don’t forget to park it back on the charger when you are done. You can do so by driving up to the charger, when you see a green outline press AND HOLD 'p' until it's parked. Make sure it is charging, otherwise try again.
* If you don't use headphones be careful about your volume and microphone placement, it might start singing, if so immediately mute your microphone and switch to headphones.
* Beamy lives in the GitLab boardroom. If there is a meeting going on in the boardroom (which you were not invited to) when you are testing out Beamy, that is great! Do not feel like you are interrupting the meeting. It actually adds to the "cool" factor that we are a 100% remote company and can interact with people from all over the world at any given time. Introduce yourself and say "Hi" to the room and move on with your tour of Beamy.
* More info about Beam can be found at [Suitable Tech](https://suitabletech.com).
* Please report any questions or issues you have about the beam in the Slack #peopleops channel.

## Company phone number
{: #phone-number}

If you need to provide the details of GitLab's contact information you can take the [address from the visiting page](/visiting) for reference; or the [mailing address](/handbook/people-operations/#addresses) of the office in the Netherlands if that is more applicable.

If a phone number is required, leave this field empty by default. If that is not possible, then use
the general number (+1-415-761-1791), but be aware that this number simply guides to a voice message that refers the caller back to contacting us via email.

## Organization code names

* Listed in Google Sheet under 'Organization code names'
* To make it easier to recognize code names we base them on classic car models.

There are two types of code names:

* **Don't mention publicly** We can use code names anywhere we regularly share items in that medium with people outside the company: issue trackers, Group Conversations, etc. For these we don't have to use code names in things that are never published: SalesForce, Zuora, Zendesk, and verbal conversations.
* **Don't write down** there are organizations that we shouldn't write down anywhere.

## Ubiquitous language

At GitLab we use [ubiquitous language](https://martinfowler.com/bliki/UbiquitousLanguage.html) to increase communication efficiency.
This is defined in [Domain-driven design](https://en.wikipedia.org/wiki/Domain-driven_design) as: "A language structured around the domain model and used by all team members to connect all the activities of the team with the software.".
We use it for activities in GitLab, even ones not implemented in software.
By having ubiquitous words to identify concepts we prevent confusion over what is meant, for example we refer to [parts of our organization](/company/team/structure/) as a function, department, or group depending on exactly what is meant.
Make sure that domains don't overlap, for example [organization size](/handbook/sales/#organization-size) and [deal size](/handbook/sales/#deal-sizes) don't reuse words to prevent overlap.
If a term is ambiguous don't use it, for example our [hiring definitons](/handbook/hiring/#definitions) have roles and vacancies but avoid the ambiguous word job.
Make sure that people can infer as much as possible from the word, for example our [subscription options](/handbook/marketing/product-marketing/tiers/) allow you to know if someone if using self-managed or GitLab.com.
Make sure terms don't overlap without clearly defining how and why, for example see our [tier definitions](/handbook/marketing/product-marketing/#definitions).
Keep terms to one or at most two words to prevent people from introducing ambiguity by shortening a term. When using two words make the first word unique because people tend to drop the second word more often.

## MECEFU terms

MECEFU is an acronym for Mutually Exclusive Collectively Exhaustive Few words Ubiquitous-language.

You pronounce it: MessiFu. Think of the great soccer player Lionel Messi and his [kung fu](https://en.wikipedia.org/wiki/Chinese_martial_arts) or soccer fu skills.

We want to use MECEFU terms to describe a domain to ensure efficient communication. MECEFU terms have 4 characteristics that help with efficiency:

1. Mutually Exclusive: nothing is referenced by more than one term
1. Collectively Exhaustive: everything is covered by one of the terms
1. Few words: the longer terms are the more likely it is people will not use all of them and cause confusion, avoid acronyms because they are hard to remember (we're open to a few words to replace MECEFU as an acronyms :)
1. Ubiquitous language: [defined above](#ubiquitous-language)

An example of a MECEFU term is our [sales segmentation](/handbook/business-ops/#segmentation):

1. Mutually Exclusive: There is no overlap between the numbers and there is a single dimension.
1. Collectively Exhaustive: Everything for 0 to infinite employees is covered.
1. Few words: Mid-market is a natural combination and SMB is abbreviated.
1. Ubiquitous language: We're not using the word 'Enterprise' which already can refer to our Enterprise Edition distribution.

One nit-pick is that the Medium of SMB and Mid of Mid-Market sound very similar.

## YouTube

See the [YouTube page](/handbook/communication/youtube).

## Knowledge Sharing

As GitLab continues to grow, sharing knowledge across the community becomes even more important. The [Knowledge Sharing page](/handbook/communication/knowledge-sharing) describes initiatives we are trying to encourage this.
