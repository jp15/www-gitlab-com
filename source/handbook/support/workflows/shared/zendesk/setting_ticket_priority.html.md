---
layout: markdown_page
title: Setting ticket priority
category: Zendesk
---

### On this page
{:.no_toc}

- TOC
{:toc}

----

## Setting Ticket Priority

If a customer submits a ticket via the web ticket form, they can choose the starting priority of the ticket - this is the 'Customer Priority' field you will see in Zendesk. On ticket creation a trigger sets the main 'Priority' field to match the 'Customer Priority' choice.

If a customer emails in a ticket it will get a Prioriy of 'Normal' (unless it is sent to the special emergency contact).

Manually setting a ticket's priority in Zendesk will change the overall ticket [SLA](/handbook/support/support-engineering/working-with-tickets.html#sla), for both the first and next replies. This allows support to prioritize tickets and update the urgency during the life of the ticket (for example the initial request may be 'High' priority and then follow up questions may need 'Low' priority.)

### Effect of Prioritization on SLA (non-premium customers)

| Priority | First/Next Reply - SLA (business hours) |
|----------|-----------------------------------------|
| Low      |  24 hours                               |
| Normal   |  8 hours (Medium)                       |
| High     |  4 hours                                |
| Urgent   |  30 mins (Emergency)                    |

### Prioritization Guidelines

Tickets should be prioritized based on the guidelines below. The priority can change at any stage in a ticket's lifecycle.

#### Low

Questions or Clarifications around features or documentation or deployments (24 hours)
Minimal or no Business Impact. Information, an enhancement, or documentation clarification is requested, but there is no impact on the operation of GitLab. Implementation or production use of GitLab is continuing and work is not impeded. Example: A question about enabling ElasticSearch.


+ Minor problems
+ Fix or workaround is known
+ Core GitLab functionality is not affected, and does not affect customer's work
+ Documentation related
+ General information questions
+ Feature requests

#### Normal (Medium)

Something is preventing normal GitLab operation (8 hours)
Some Business Impact. Important GitLab features are unavailable, or somewhat slowed, but a workaround is available. GitLab use has a minor loss of operational functionality, regardless of the environment or usage. Example: A Known bug impacts the use of GitLab, but a workaround is successfully being used as a temporary solution.

+ Minor problems
+ GitLab is not functioning correctly
+ Customers may be inconvenienced, but are able to continue working
+ Low impact on general GitLab usage

#### High

GitLab is Highly Degraded (4 hours)
Significant Business Impact. Important GitLab features are unavailable, or extremely slowed, with no acceptable workaround. Implementation or production use of GitLab is continuing; however, there is a serious impact on productivity. Example: CI Builds are erroring and not completing successfully, and the software release process is significantly affected.

+ Major problems
+ Partial loss of GitLab functionality
+ Customer's ability to operate is seriously affected
+ High impact on GitLab usage

#### Urgent (Emergency)

GitLab is unavailable or completely unusable (30 Minutes)
A GitLab server or cluster in production is not available, or is otherwise unusable. An emergency ticket can be filed and our On-Call Support Engineer will respond within 30 minutes. Example: GitLab showing 502 errors for all users.

+ Major emergencies only
+ Full loss of GitLab functionality, service is completely unavailable
+ GitHost (GitLab Hosted) instance down
+ Customers cannot continue to work at all
