---
layout: markdown_page
title: "Category Vision - Dependency Proxy"
---

- TOC
{:toc}

## Dependency Proxy

Users or organizations who deploy complex pieces of software often depend on a large number of external packages. Those packages have source code written by external and often unknown parties. Also, they need to be retrieved from a bunch of different places. This sets the user up for an unreliable code supply chain with potential security and availability risks due to the fact that there is no layer or step in between mixing external code and internal code. An additional or step which could automatically scan and back up said code. Additionally, it can have an effect upon the speed of pipeline builds as external code needs to be re-fetched again and again. Being able to store and potentially cache that on-site is sure to bring speed improvements to pipeline builds.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Dependency%20Proxy)
- [Overall Vision](https://about.gitlab.com/direction/package/)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

### Usecases listed

1. Provide a single method of reaching these upstream package management utilities, in the event, they are not otherwise reachable. This is commonly due to network restrictions. 
1. Letting proxy packages act as a cache for increased pipeline build speeds. 
1. Verify package integrity from one single place. See what has been changed and test them for security vulnerabilities (part of black duck model). 
1. Filtering of the available upstream packages to a list of approved packages, whitelisting them. 
1. Tracking which dependencies are utilized by which projects when pulled through the proxy. (Perhaps when authenticated with a `CI_JOB_TOKEN`.) 
1. Auditing logs in order to find out exactly what happened and with what code exactly. 
1. Being able to operate fully cut off the internet with local dependencies.
1. Beyond blindly caching all packages, or relying on a manually-updated whitelist, enforce policies at the proxy layer. e.g. scan packages for licenses and only allow packages with compatible licenses. Run container or dependency scanning on the package, and only allow proxying of packages without any S1 vulnerabilities.

## What's next & why

The current maturity stage of this category is targeting [minimal](https://about.gitlab.com/handbook/product/categories/maturity/) which implicates that we are just starting out. First priority is to get down the MVC which preferably compliments a major existing feature which is widely used to be more powerful. In that sense implementing proxy packages for our docker registry https://gitlab.com/gitlab-org/gitlab-ee/issues/7934 to better enable Auto DevOps on on-premise GitLab installations is a sound first step since it has the most usage within GitLab, and can also help improve other features like Auto DevOps. This allows for a wider audience to leverage the MVC, and provide important feedback.

### Prioritization, Motivation, Confidence level

Once the [MVC](https://gitlab.com/gitlab-org/gitlab-ee/issues/7934) has shipped we have the first checkpoint which allows us to receive feedback, gather usage data and more confidently set out next steps. Large impact improvements will be preferred afterwards.

This is part of our 2019 vision due to demand from enterprise customers.

## Competitive landscape

* [Artifactory](https://www.jfrog.com/confluence/display/RTF/Docker+Registry#DockerRegistry-RemoteDockerRepositories)
* [Nexus](https://help.sonatype.com/repomanager3/configuration/repository-management#RepositoryManagement-ProxyRepository)
* [OpenShift (whitelists specific registries)](https://docs.openshift.com/container-platform/3.4/install_config/registry/extended_registry_configuration.html#whitelisting-docker-registries)
* [Quai container registries](https://www.openshift.com/products/quay)
* [Grafeas (metadata validity)](https://grafeas.io/)

Jfrog, Nexus, and most competitors have a lead on us in terms of shear quantity of package repositories they already support. Their approach to proxy repositories is one of having an additional repository. We should take this same approach but with the management structure that GitLab provides in order to make this much more accessible and simple to understand.

Grafeas shines light upon automatic whitelisting and general package management opportunities. We can further inform our whitelisting rulesets with data gathered from our security tests.

Our added value comes from combining this with our own CI/CD services. Speed improvements and having everything on-premise is a big win.

## Analyst landscape

N/A (Not included in quadrant or wave by [Gartner](https://docs.google.com/spreadsheets/d/1fN0hc2kmGai1S76PMeU1Nmfs-Zat3GcUUn3sBX4RiTA/edit#gid=1149824002) and/or [Forester](https://docs.google.com/spreadsheets/d/1n4M_ju_f99-0RmbgDPE1NZUB5s9eUgcQNdsEo-YbUnk/edit#gid=56346071))

## Top Customer Success/Sales issue(s)

N/A (None as of yet)

## Top user issue(s)

N/A (This is a new category and is yet to receive wide input from the community)

## Top internal customer issue(s)

- Government/Federal customers

## Top Vision Item(s)

The MVC issue [gitlab-ee#7934](https://gitlab.com/gitlab-org/gitlab-ee/issues/7934) will shine new light on the impact this feature has and has a wide reach by making a proxy repository possible for docker images.