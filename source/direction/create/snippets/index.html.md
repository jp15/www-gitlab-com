---
layout: markdown_page
title: "Category Vision - Snippets"
---

- TOC
{:toc}

## Snippets

<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Please include usecases, personas, 
and user journeys into this section. -->

Snippets allow fragments of code to be shared between people.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=snippets)
- [Epic List](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=snippets)
- [Overall Vision](/direction/create/)

Please reach out to PM James Ramsay ([E-Mail](jramsay@gitlab.com)
[Twitter](https://twitter.com/jamesramsay)) if you'd like to provide feedback or ask
questions about what's coming.

## Target Audience and Experience
<!-- An overview of the personas involved in this category. An overview 
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.-->

## What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

**Next: [Version controlled snippets](https://gitlab.com/groups/gitlab-org/-/epics/239)** - Snippets should be built on top of a Git repository so that they can be versioned and support multiple files. Support for [multiple files](https://gitlab.com/gitlab-org/gitlab-ce/issues/14844) and [versioning](https://gitlab.com/gitlab-org/gitlab-ce/issues/13426) are very frequently requested, ranked 13 and 19 of all issues on the GitLab CE issue tracker.

## Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

Snippets capabilities are currently most close to [Pastebin](https://pastebin.com).

The primary competitor is Gists which is a no frills way to share snippets of code that are version controlled with Git. They allow multiple files to be shared (e.g. a mix of Javascript, CSS, HTML), forked (e.g. I can make a copy of something interesting I see), and remixed (e.g. I can take a cool snippet and tweak it for my own use). By being a Git repository, they are familiar to Git users and can be used in a variety of interesting ways from the command line.

A range of clever tools have since been built on top of Gists including:

- [Blocks](https://bl.ocks.org/)
- [Gist.io](http://gist.io/)

Atlassian has [Bitbucket Snippets](https://confluence.atlassian.com/bitbucket/snippets-719095082.html) which is very similar to Gists, supporting Git and Mercurial.

If Snippets were combined with the Web IDE with client-side and server-side evaluation it would also compete with:

- Javascript sandboxes: [Codesandbox](https://codesandbox.io), [JSBin](https://jsbin.com), [JSFiddle](https://jsfiddle.net), [Codepen](https://codepen.io)
- Server-side sandboxes: [Codesandbox](https://codesandbox.io), [repl.it](https://repl.it)

## Market Research
<!-- This section should link or highlight any relevant market research you've done that justifies our
entry into the market for the particular category. -->

## Business Opportunity
<!-- This section should highlight the business opportunity highlighted by the particular category. -->

## Analyst Landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

Snippets and pastebins are not currently an area of analyst interest.

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

- [Version controlled snippets](https://gitlab.com/groups/gitlab-org/-/epics/239) are important to enterprise customers because they provide an audit trail of changes. The current state of snippets means they are completely uncontrolled.

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

- [Snippets with multiple files](https://gitlab.com/gitlab-org/gitlab-ce/issues/14844) (solved by version controlled snippets)
- [Version controlled snippets](https://gitlab.com/groups/gitlab-org/-/epics/239)

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

Snippets are not used regularly internally.

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->

- [Version controlled snippets](https://gitlab.com/groups/gitlab-org/-/epics/239)
