---
layout: markdown_page
title: "Development Metrics Working Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property | Value |
|----------|-------|
| Date Created | February 26, 2019 |
| Date Ended   | TBD |
| Slack        | [#wg_dev-metrics](https://gitlab.slack.com/messages/CGQ4R90F5) (only accessible from within the company) |
| Google Doc   | [Development Metrics Working Group Agenda](https://docs.google.com/document/d/1Y50uhpRW0zSGWI-TzPxHnwEHyOl7uWiyCzXtpRJd1_E/edit) (only accessible from within the company) |

## Business Goal

To improve the productivity of the Development Department within the Engineering Function by creating and interpreting new development metrics and implementing improvements.

## Exit Criteria

* 20% increase in development department throughput
* Defined KPIs for the development organization in a dashboard
* Training for managers on KPIs and interventions they can make

## Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Facilitator           | Christopher Lefelhocz | Senior Director of Development |
| Quality Lead          | Mek Stittri           | Interim Director of Quality    |
| Member                | Dalia Havens          | Director of Engineering, Ops   |
| Member                | Dennis Tang           | Frontend Engineering Manager   |
| Member                | Clement Ho            | Frontend Engineering Manager   |
| Executive Stakeholder | Eric Johnson          | VP of Engineering              |
